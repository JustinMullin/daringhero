package xyz.jmullin.daringhero

import java.io.File

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.github.kittinunf.fuel.Fuel
import xyz.jmullin.drifter.rendering.Draw
import java.io.BufferedInputStream
import java.io.StringWriter


object Icons {
    val imageIconsRoot = File("image/itemIcons")
    val avatarsRoot = File("image/avatars")

    val missingImages = listOf<String>()

    val existingIcons by lazy {
        imageIconsRoot.listFiles().filter { it.name.endsWith(".png") }.map { file ->
            file.name.dropLast(4) to fileToSprite(file)
        }.let { mutableMapOf(*it.toTypedArray()) }
    }

    val existingAvatars by lazy {
        avatarsRoot.listFiles().map { file ->
            file.name.dropLast(4) to fileToSprite(file)
        }.let { mutableMapOf(*it.toTypedArray()) }
    }

    private fun fileToSprite(file: File) = Sprite(Texture(Gdx.files.absolute(file.path)))

    private fun downloadIcon(url: String, filename: String) = File(filename).apply {
        Fuel.download(url).destination { _, _ -> this }.response()
    }

    private fun formatItemName(itemName: String): String {
        if(itemName == "Rail Gun") return "slug-shooter"
        if(itemName == "Cthulhu") return "cthulu"
        if(itemName == "Coin A") return "co-presidents-coin"
        if(itemName == "Coin B") return "co-presidents-coin"
        if(itemName == "Coin C") return "co-presidents-coin"
        if(itemName == "Coin D") return "co-presidents-coin"
        return itemName.replace("\\s".toRegex(), "-").replace("[':]".toRegex(), "").toLowerCase()
    }

    fun getIcon(itemName: String): Sprite {
        val name = formatItemName(itemName)
        if(missingImages.contains(itemName)) return Draw.fill
        return existingIcons.getOrPut(name, {
            try {
                println("Loading icon for $itemName [http://nerdquest.nerderylabs.com/assets/media/images/$name.svg]...")
                val file = downloadIcon("http://nerdquest.nerderylabs.com/assets/media/images/$name.svg", "image/itemIcons/$name.svg")
                val process = Runtime.getRuntime().exec("java -Djava.awt.headless=true -jar ../../core/lib/batik/batik-rasterizer-1.9.jar ${file.absolutePath} -w 256 -h 256")
                val errorMessage = convertStreamToString(process.errorStream)
                if(errorMessage.isNotBlank()) println("ERROR: $errorMessage")
                fileToSprite(File("image/itemIcons/$name.png"))
            } catch (e: Exception) {
                Draw.fill
            }
        })
    }

    fun getAvatar(playerName: String, url: String): Sprite {
        return existingAvatars.getOrPut(playerName, {
            try {
                println("Loading avatar image for $playerName [$url]...")
                val extension = url.takeLast(4)
                downloadIcon(url, "image/avatars/$playerName$extension")
                fileToSprite(File("image/avatars/$playerName$extension"))
            } catch (e: Exception) {
                Draw.fill
            }
        })
    }

    fun convertStreamToString(stream: java.io.InputStream): String {
        val s = java.util.Scanner(stream).useDelimiter("\\A")
        return if (s.hasNext()) s.next() else ""
    }
}
