package xyz.jmullin.daringhero.shader

import xyz.jmullin.drifter.rendering.ShaderSet

object HeroShaders {
    val cooldownShader = ShaderSet("cooldown", "default")
}