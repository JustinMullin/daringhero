package xyz.jmullin.daringhero

import com.github.sheigutn.pushbullet.Pushbullet
import com.github.sheigutn.pushbullet.items.push.sendable.defaults.SendableNotePush
import java.time.LocalTime

object Notify {
    val notifier = Pushbullet("o.LLTmt4nnOmipOQetkn6MyejYKyoNLTAL")
}

fun notify(title: String, body: String): Unit {
    if(LocalTime.now().isAfter(LocalTime.of(7, 0)) &&
        LocalTime.now().isBefore(LocalTime.of(22, 0))) {
        Notify.notifier.push(SendableNotePush(title, body))
    }

}