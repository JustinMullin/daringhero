package xyz.jmullin.daringhero

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import xyz.jmullin.daringhero.Stage.Postprocessing
import xyz.jmullin.daringhero.Stage.Ui
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.drifter.application.DrifterScreen
import xyz.jmullin.drifter.extensions.V2
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.InputMultiplexer
import xyz.jmullin.daringhero.entity.*
import xyz.jmullin.drifter.extensions.gameH
import xyz.jmullin.drifter.extensions.gameW

object GUI : DrifterScreen(Color.BLACK) {
    val world = newLayer2D(1, Game.size, true, Postprocessing) {

    }

    val ui = newLayer2D(2, V2(1400f, 800f), true, Ui) {
        if(DaringHero.devMode) {
            //add(FPSDisplay(Assets.prototype14, Color.WHITE, Color.CLEAR, V2(1f, 1f)))
        }
    }

    val inventoryPanelWidth = 800
    //    val itemPanelWidth = inventoryPanelWidth - abilityPanelWidth
    val statusPanelHeight = 200
    val chargePanelHeight = 40
    val questPanelHeight = 32f
    val questButtonWidth = 32f
    val toggleButtonWidth = chargePanelHeight
    val abilityPanelWidth = toggleButtonWidth*2f

    var page = 0

    init {
        ui.add(InventoryPanel.apply {
            position.set(V2(0, statusPanelHeight + chargePanelHeight))
            size.set(V2(inventoryPanelWidth - abilityPanelWidth, gameH() - statusPanelHeight - chargePanelHeight))
            load()
        })
        ui.add(AbilityPanel.apply {
            position.set(V2(inventoryPanelWidth - abilityPanelWidth, statusPanelHeight + chargePanelHeight))
            size.set(V2(abilityPanelWidth, gameH() - statusPanelHeight - chargePanelHeight - toggleButtonWidth*6f))
            load()
        })
        AbilityPanel.classes.sliding(2).forEachIndexed { i, row ->
            row.forEachIndexed { j, className ->
                ui.add(ClassButton(className).apply {
                    position.set(V2(inventoryPanelWidth - abilityPanelWidth + j * toggleButtonWidth, gameH() - (i + 1) * toggleButtonWidth))
                    size.set(V2(toggleButtonWidth, toggleButtonWidth))
                })
            }
        }
        ui.add(LevelUpButton.apply {
            position.set(V2(inventoryPanelWidth - abilityPanelWidth, gameH() - toggleButtonWidth*6f))
            size.set(V2(toggleButtonWidth*2f, toggleButtonWidth))
        })
        ui.add(PlayerPanel.apply {
            position.set(V2(inventoryPanelWidth, 0))
            size.set(V2(gameW()- inventoryPanelWidth, gameH()))
        })
        ui.add(LogPanel.apply {
            position.set(V2(0, 0))
            size.set(V2(inventoryPanelWidth, statusPanelHeight - questPanelHeight))
        })
        ui.add(StatusIndicator("L", "leaderboard", { Controller.timeSinceLastRefresh / Controller.LeaderboardRefreshDelay.toFloat() }).apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*2f, statusPanelHeight))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(StatusIndicator("U", "use").apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*2f, statusPanelHeight+toggleButtonWidth / 2f))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(StatusIndicator("P", "points", { Controller.timeSinceLastPost / Controller.PointsDelay.toFloat() }).apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*1.5f, statusPanelHeight))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(StatusIndicator("A", "ability").apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*1.5f, statusPanelHeight+toggleButtonWidth / 2f))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(StatusIndicator("S", "status").apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth, statusPanelHeight))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(StatusIndicator("Q", "quest").apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth, statusPanelHeight+toggleButtonWidth / 2f))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(StatusIndicator("E", "effect").apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*0.5f, statusPanelHeight))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(StatusIndicator("C", "class").apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*0.5f, statusPanelHeight+toggleButtonWidth / 2f))
            size.set(V2(toggleButtonWidth / 2f))
        })
        ui.add(ChargePanel(V2(0f, statusPanelHeight), V2(inventoryPanelWidth - toggleButtonWidth*5f, chargePanelHeight)))
        ui.add(ToggleButton(Color.GREEN, "R", {Controller.isRunning}, {Controller.isRunning = !Controller.isRunning}).apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*5f, statusPanelHeight))
            size.set(V2(toggleButtonWidth.toFloat()))
        })
        ui.add(ToggleButton(Color.BLUE, "C", {Controller.cheatEnabled}, {Controller.cheatEnabled = !Controller.cheatEnabled}).apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*4f, statusPanelHeight))
            size.set(V2(toggleButtonWidth.toFloat()))
        })
        ui.add(ToggleButton(Color.RED, "I", {Controller.placeholderOption}, {Controller.placeholderOption = !Controller.placeholderOption}).apply {
            position.set(V2(inventoryPanelWidth - toggleButtonWidth*3f, statusPanelHeight))
            size.set(V2(toggleButtonWidth.toFloat()))
        })
        ui.add(QuestButton.apply {
            position.set(V2(0f, statusPanelHeight - questPanelHeight))
            size.set(V2(questButtonWidth, questPanelHeight))
        })
        ui.add(QuestPanel.apply {
            position.set(V2(questButtonWidth, statusPanelHeight - questPanelHeight))
            size.set(V2(inventoryPanelWidth - questButtonWidth, questPanelHeight))
        })

        ui.add(TooltipRenderer.apply {
            position.set(V2(4f, 4f))
            size.set(V2(inventoryPanelWidth - 8f, statusPanelHeight - questPanelHeight - 8f))
        })
        ui.add(TinyTooltip)
//        ui.add(Console.apply {
//            position.set(V2(0f, -38f))
//            size.set(V2(gameW(), 30f))
//        })

        Controller.execute()
    }

    override fun show() {
        super.show()

//        Gdx.input.inputProcessor = InputMultiplexer(ui, Console)
    }

    override fun keyDown(keycode: Int): Boolean {
        when(keycode) {
            Keys.UP -> {
                page = Math.max(0, page - 1)
                Controller.lastRefresh = 0
            }
            Keys.DOWN -> {
                page += 1
                Controller.lastRefresh = 0
            }
            Keys.ESCAPE -> Gdx.app.exit()
            else -> {
                if(keycode >= Keys.NUM_1 && keycode <= Keys.NUM_9) {
                    page = keycode - Keys.NUM_1
                    Controller.lastRefresh = 0
                }
            }
        }

        return super.keyDown(keycode)
    }

}