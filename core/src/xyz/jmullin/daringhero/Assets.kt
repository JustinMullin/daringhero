package xyz.jmullin.daringhero

import xyz.jmullin.drifter.assets.DrifterAssets

@Suppress("UNUSED")
object Assets : DrifterAssets("daring-hero") {
    val prototype14 by font
    val prototype18 by font
    val petMe12 by font
    val fontMedium by font
    val fontSmall by font
    val fontTiny by font
    val fontMiniscule by font

    val widget by sprite
}