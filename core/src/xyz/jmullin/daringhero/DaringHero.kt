package xyz.jmullin.daringhero

import xyz.jmullin.drifter.application.DrifterGame

object DaringHero : DrifterGame("daring-hero", Assets) {
    override fun create() {
        super.create()

        setScreen(GUI)
    }
}
