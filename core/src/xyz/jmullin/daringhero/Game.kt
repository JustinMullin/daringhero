package xyz.jmullin.daringhero

import xyz.jmullin.drifter.extensions.V2

object Game {
    val size = V2(1400f, 800f)
}