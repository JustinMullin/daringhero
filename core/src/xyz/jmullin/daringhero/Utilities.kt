package xyz.jmullin.daringhero

import xyz.jmullin.daringhero.bot.Controller
import java.time.LocalTime
import java.time.format.DateTimeParseException

fun String.truncate(length: Int) = {
    if (this.length > length - 3) take(length - 3) + "..."
    else this
}()

val Int.hour: Int get() = this * 1000 * 60 * 60
val Int.hours: Int get() = this * 1000 * 60 * 60
val Int.minute: Int get() = this * 1000 * 60
val Int.minutes: Int get() = this * 1000 * 60
val Int.second: Int get() = this * 1000
val Double.second: Int get() = (this * 1000).toInt()
val Int.seconds: Int get() = this * 1000
val Double.seconds: Int get() = (this * 1000).toInt()
val Int.millisecond: Int get() = this
val Int.milliseconds: Int get() = this

fun <T> List<T>.sliding(window: Int) = if(window == 0) listOf<List<T>>() else
    (0..(size / window)).map { subList(it * window, Math.min((it+1) * window, size)) }

fun String.toTimeTry(): LocalTime? = try {
    LocalTime.parse(this, Controller.df).minusHours(5)
} catch (e: DateTimeParseException) {
    println(e)
    null
}

fun String.toIntTry(): Int? = try {
    toInt()
} catch (e: NumberFormatException) {
    null
}

fun String.nullIfEmpty() = if(isEmpty()) null else this