package xyz.jmullin.daringhero.bot.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

import java.math.BigInteger

data class Catalog(val entries: List<CatalogEntry>)

data class CatalogEntry(val category: String?, val duration: Int?, val name: String, val rarity: Int, val description: String)

data class Abilities(val entries: List<Ability>)

data class Ability(val name: String, val classOrigin: String, val duration: Int?)

data class Inventory(val items: List<InventoryEntry>)

data class InventoryEntry(val item: String, val id: UUID, val foundAt: Long = System.currentTimeMillis())

data class Badge(@JsonProperty("BadgeName") val name: String) {
    override fun toString() = name
}

data class Item(@JsonProperty("Name") val name: String,
                @JsonProperty("Id") val id: UUID,
                @JsonProperty("Rarity") val rarity: Int,
                @JsonProperty("Description") val description: String) {
    val rarityText: String get() = when(rarity) {
        1 -> "Common"
        2 -> "Uncommon"
        3 -> "Rare"
        4 -> "Epic"
        5 -> "Legendary"
        6 -> "Unique"
        else -> "Unknown"
    }
}

data class Event(val Messages: List<String>, val Item: Item?, val Points: BigInteger, val Effects: List<String>, val Badges: List<Badge>)

data class UseEvent(val Messages: List<String>, val Points: BigInteger, val TargetName: String)

@JsonIgnoreProperties(ignoreUnknown = true)
data class GenericEvent(@JsonProperty("Messages") val messages: List<String>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class EffectInfo(@JsonProperty("Timestamp") val timestamp: String,
                      @JsonProperty("Creator") val creator: String,
                      @JsonProperty("Targets") val targets: String)

data class EffectDetails(@JsonProperty("EffectName") val name: String,
                         @JsonProperty("EffectType") val type: String,
                         @JsonProperty("Duration") val duration: EffectDuration,
                         @JsonProperty("VoteGain") val points: String,
                         @JsonProperty("Description") val description: String)

data class EffectDuration(@JsonProperty("Case") val case: String,
                          @JsonProperty("Fields") val child: List<EffectDurationDetails>?)

data class EffectDurationDetails(@JsonProperty("Case") val case: String,
                                 @JsonProperty("Fields") val fields: List<String>?)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ProfileSummary(@JsonProperty("PlayerName") val playerName: String,
                          @JsonProperty("Points") val points: BigInteger,
                          @JsonProperty("AvatarUrl") val avatarUrl: String,
                          @JsonProperty("Badges") val badges: List<Badge>,
                          @JsonProperty("Effects") val effects: List<String>,
                          @JsonProperty("Title") val title: String,
                          @JsonProperty("HasActiveQuest") val hasActiveQuest: Boolean,
                          @JsonProperty("ClassName") val className: String?,
                          @JsonProperty("ClassLevel") val classLevel: Int?)