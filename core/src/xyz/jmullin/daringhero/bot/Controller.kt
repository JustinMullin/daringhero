package xyz.jmullin.daringhero.bot

import com.badlogic.gdx.Gdx
import com.fasterxml.jackson.core.type.TypeReference
import xyz.jmullin.daringhero.*
import xyz.jmullin.daringhero.bot.model.Ability
import xyz.jmullin.daringhero.bot.model.ProfileSummary
import xyz.jmullin.daringhero.entity.*
import xyz.jmullin.daringhero.entity.InventoryPanel.ItemSelection
import xyz.jmullin.drifter.extensions.rElement
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import kotlin.concurrent.thread

object Controller {
    val NormalItemDelay = 61.seconds
    val VampireItemDelay = 15100.milliseconds
    val itemDelay: Int get() = if (PlayerPanel.myBadges.find { it.name == "Vampire" } != null &&
                                   PlayerPanel.myEffects.contains("Vampirism")) VampireItemDelay else NormalItemDelay

    var placeholderOption = false
    var isRunning = true
    var cheatEnabled = false

    val CheatDelay = 200.milliseconds
    val PointsDelay: Int get() = if(cheatEnabled/* &&
        (!PlayerPanel.myEffects.contains("Vampirism") || !PlayerPanel.myEffects.contains("Lycanthropy"))*/) {
        CheatDelay
    } else 1050.milliseconds
    val LeaderboardRefreshDelay = 11.seconds
    val SelfEffectsRefreshDelay = 5.seconds
    val WatchEffectsRefreshDelay = 2.seconds

    val prefs = Gdx.app.getPreferences("daringhero")!!

    var lastAbilityUse = Api.Mapper.readValue<Map<String, Long>>(
        prefs.getString("lastAbilityUse", "{}"), object : TypeReference<Map<String, Long>>() {})!!
    fun timeSinceLastAbilityUse(abilityName: String): Long {
        return lastAbilityUse[abilityName]?.let { lastUse -> System.currentTimeMillis() - lastUse } ?: Long.MAX_VALUE
    }

    var lastIndividualItemUse = Api.Mapper.readValue<Map<String, Long>>(
        prefs.getString("lastIndividualItemUse", "{}"), object : TypeReference<Map<String, Long>>() {})!!
    fun timeSinceLastIndividualItemUse(itemName: String): Long {
        return lastIndividualItemUse[itemName]?.let { lastUse -> System.currentTimeMillis() - lastUse } ?: Long.MAX_VALUE
    }

    var lastItemUse = prefs.getLong("lastItemUse", 0)
    val timeSinceLastItemUse: Long get() = System.currentTimeMillis() - lastItemUse

    var currentQuest: String? = prefs.getString("currentQuest").nullIfEmpty()
    var questEnds = prefs.getLong("questEnds", 0)
    var questLength = prefs.getLong("questLength", 1)
    var lastQuestAccepted = prefs.getLong("lastQuestAccepted", 0)
    val timeSinceLastQuestAccepted: Long get() = System.currentTimeMillis() - lastQuestAccepted
    val timeRemainingInQuest: Long get() = Math.max(0, questEnds - System.currentTimeMillis())

    var classLevels = Api.Mapper.readValue<Map<String, Int>>(
        prefs.getString("classLevels", "{}"), object : TypeReference<Map<String, Int>>() {})!!
    var currentClass: String = "Artisinal Fungal Farmer"

    var lastPost = 0L
    val timeSinceLastPost: Long get() = System.currentTimeMillis() - lastPost

    var lastRefresh = 0L
    val timeSinceLastRefresh: Long get() = System.currentTimeMillis() - lastRefresh

    var lastSelfCheck = 0L
    val timeSinceLastSelfCheck: Long get() = System.currentTimeMillis() - lastSelfCheck

    var lastWatchCheck = 0L
    val timeSinceLastWatchCheck: Long get() = System.currentTimeMillis() - lastWatchCheck

    fun execute() {
        thread {
            while (true) {
                gameLoop()
                if (timeSinceLastRefresh > LeaderboardRefreshDelay) {
                    lastRefresh = System.currentTimeMillis()
                    refreshLeaderboard()
                }
                if (timeSinceLastSelfCheck > SelfEffectsRefreshDelay) {
                    lastSelfCheck = System.currentTimeMillis()
//                    refreshSelfEffects()
                }
                if (timeSinceLastWatchCheck > WatchEffectsRefreshDelay) {
                    lastWatchCheck = System.currentTimeMillis()
                    refreshWatchEffects()
                }
                Thread.sleep(5)
            }
        }
    }

    fun gameLoop() {
        val itemQueued = InventoryPanel.itemSelected != null && PlayerPanel.targetSelected != null

        InventoryPanel.itemButtons.forEach { itemButton ->
            itemButton.timeRemaining = (itemButton.item.duration?.seconds ?: 0) - timeSinceLastIndividualItemUse(itemButton.item.name)
        }

        val autoUseItems = InventoryPanel.itemButtons.filter { it.autoUse }
        val nearestAutoUse = if(autoUseItems.isEmpty()) {
            null
        } else {
            val nearest = autoUseItems.minBy { it.timeRemaining }
            nearest?.timeRemaining?.let {
                it to nearest
            }
        }

        QuestButton.timeRemaining = QuestButton.duration - timeSinceLastQuestAccepted

        AbilityPanel.buttons.forEach { abilityButton ->
            val ability = abilityButton.ability
            abilityButton.timeRemaining = (ability.duration?.seconds ?: 0) - timeSinceLastAbilityUse(ability.name)

            if(abilityButton.selected || abilityButton.autoUse) {
                if(abilityButton.timeRemaining <= 0f && PlayerPanel.targetSelected != null) {
                    val success = useAbility(ability, PlayerPanel.targetSelected!!)

                    if(success) {
                        AbilityPanel.unselect()
                        PlayerPanel.unselect()
                        lastAbilityUse += ability.name to System.currentTimeMillis()
                        prefs.putString("lastAbilityUse", Api.Mapper.writeValueAsString(Controller.lastAbilityUse))
                        prefs.flush()
                    } else {
                        lastAbilityUse += ability.name to (System.currentTimeMillis() - (ability.duration?.seconds ?: 0) + 1500)
                        prefs.putString("lastAbilityUse", Api.Mapper.writeValueAsString(Controller.lastAbilityUse))
                        prefs.flush()
                    }
                }
            }
        }

        if (timeSinceLastPost > PointsDelay) {
            if (timeSinceLastItemUse >= itemDelay && nearestAutoUse != null && nearestAutoUse.first <= 0 ) {
                val button = nearestAutoUse.second
                LogPanel.append("Using ${button.item.name}...")

                val success = useItem(ItemSelection(button.item, InventoryPanel.inventory.items.find { it.item == button.item.name }!!.id), "jmullin")

                if(success) {
                    lastIndividualItemUse += button.item.name to System.currentTimeMillis()
                    prefs.putString("lastIndividualItemUse", Api.Mapper.writeValueAsString(Controller.lastIndividualItemUse))
                    prefs.flush()
                    lastItemUse = System.currentTimeMillis()
                    ChargePanel.tick = 0f
                    prefs.putLong("lastItemUse", Controller.lastItemUse)
                    prefs.flush()
                } else {
                    Controller.lastItemUse = System.currentTimeMillis() - Controller.itemDelay + 1700
                    Controller.prefs.putLong("lastItemUse", Controller.lastItemUse)
                    Controller.prefs.flush()
                }

                lastPost = System.currentTimeMillis()
            } else if (timeSinceLastItemUse >= itemDelay && itemQueued) {
                if (nearestAutoUse == null || nearestAutoUse.first > itemDelay) {
                    val item = InventoryPanel.itemSelected!!
                    LogPanel.append("Using ${item.catalog.name}...")

                    val player = PlayerPanel.targetSelected!!
                    val success = useItem(item, player)

                    if(success) {
                        InventoryPanel.unselectItems()
                        PlayerPanel.unselect()

                        lastIndividualItemUse += item.catalog.name to System.currentTimeMillis()
                        prefs.putString("lastIndividualItemUse", Api.Mapper.writeValueAsString(Controller.lastIndividualItemUse))
                        prefs.flush()

                        lastItemUse = System.currentTimeMillis()
                        ChargePanel.tick = 0f
                        prefs.putLong("lastItemUse", Controller.lastItemUse)
                        prefs.flush()
                    } else {
                        Controller.lastItemUse = System.currentTimeMillis() - Controller.itemDelay + 1700
                        Controller.prefs.putLong("lastItemUse", Controller.lastItemUse)
                        Controller.prefs.flush()
                    }

                    lastPost = System.currentTimeMillis()
                }
            } else {
                if (!(itemQueued && timeSinceLastItemUse > itemDelay - PointsDelay) &&
                    (nearestAutoUse == null || nearestAutoUse.first > PointsDelay) &&
                    isRunning) {// && PlayerPanel.myScore < 1337133713370l) {
                    Api.search()
                    lastPost = System.currentTimeMillis()
                }
            }
        }
    }

    fun refreshLeaderboard() {
        try {
            Api.getLeaderboard(GUI.page, { leaderboard ->
                if(leaderboard.isEmpty()) return@getLeaderboard
                StatusIndicator.success("leaderboard", "Fetched the leaderboard successfully.")
                val myEntry = leaderboard.find { it.playerName == "jmullin" }
                val adjustedLeaderboard =
                    if (myEntry == null) {
                        val myRank = myEntry?.let { entry -> leaderboard.indexOf(entry) } ?: -1

                        val me = ProfileSummary(
                            playerName = "jmullin",
                            points = PlayerPanel.myScore,
                            effects = PlayerPanel.myEffects,
                            badges = PlayerPanel.myBadges,
                            title = PlayerPanel.myTitle,
                            avatarUrl = "https://lh6.googleusercontent.com/-4O94sKa-QEU/AAAAAAAAAAI/AAAAAAAAC5A/tfvhaVH38dU/photo.jpg",
                            hasActiveQuest = false,
                            className = Controller.currentClass,
                            classLevel = Controller.classLevels[Controller.currentClass] ?: 0
                        )

                        if (leaderboard.first().points <= PlayerPanel.myScore) {
                            listOf(IndexedValue(myRank, me)) + leaderboard.withIndex()
                        } else {
                            leaderboard.withIndex() + IndexedValue(myRank, me)
                        }
                    } else {
                        leaderboard.withIndex()
                    }

                PlayerPanel.updateButtons(adjustedLeaderboard.toList())
            }, { code ->
                if(code == 999) {
                    Api.timeouts += 1
                    if(Api.timeouts > 5) {
                        LogPanel.append(rElement(listOf(
                            "...",
                            "...is anyone there?...",
                            "...hello?...",
                            "...the server is sleeping...",
                            "...zzzZZZzZZzzzZZz..."
                        )))
                        Api.timeouts -= 5
                    }
                } else {
                    lastRefresh = System.currentTimeMillis() - LeaderboardRefreshDelay + 1500
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun refreshSelfEffects() {
        try {
//            Api.getSelfEffects()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun refreshWatchEffects() {
        try {
//            Api.getEffects()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun useItem(item: ItemSelection, player: String): Boolean {
        if (InventoryPanel.inventory.items.find { it.id == item.id } != null) {
            return Api.use(item.catalog.name, item.id, player)
        } else {
            Api.write("data/oops.txt", "Missing item with ID ${item.id}", true)
            return false
        }
    }

    fun useAbility(ability: Ability, player: String): Boolean {
        return Api.useAbility(ability.name, player)
    }

    val newQuestPattern = listOf(
        "Your quest, if you choose to accomplish it, is to (.*) within (\\d+) (minute|hour)s?.",
        "Bestowed unto you is thine quest. Ye must (.*) within (\\d+) (minute|hour)s?."
    ).map { it.toRegex() }
    val finishedQuestPattern = listOf(
        "You accomplished your quest of '(.*)'!",
        "The bards doth sing of your conquest of '(.*)'"
    ).map { it.toRegex() }
    val failedQuestPattern = listOf(
        "You failed to accomplish your quest of '(.*)'.",
        "Thy failure rings about the land. Ye did not complete '(.*)'"
    ).map { it.toRegex() }
    val prematureQuestStart = listOf(
        "You cannot acquire a new quest until (\\S+).*",
        "Thy new venture be unavailble until (\\S+).*"
    ).map { it.toRegex() }
    val df = DateTimeFormatter.ofPattern("h:mma")
    
    fun handleQuestMessages(messages: List<String>) {
        if(messages.isEmpty()) return

        QuestPanel.nextQuestTime = ""

        newQuestPattern.mapNotNull { it.matchEntire(messages.first())?.groupValues }.firstOrNull()?.let { (_, questName, time, timeType) ->
            LogPanel.append("New quest accepted: '$questName.'")
            lastQuestAccepted = System.currentTimeMillis()
            prefs.putLong("lastQuestAccepted", lastQuestAccepted)

            time.toIntTry()?.let {
                questEnds = System.currentTimeMillis() + if(timeType == "minute") it.minutes else it.hours
                questLength = it.minutes.toLong()
                prefs.putLong("questEnds", questEnds)
                prefs.putLong("questLength", questLength)
            }
            if(messages.size > 1) {
                currentQuest = messages[1]
                prefs.putString("currentQuest", currentQuest)
            }
        } ?: finishedQuestPattern.mapNotNull { it.matchEntire(messages.first())?.groupValues }.firstOrNull()?.let { (_, questName) ->
            LogPanel.append("Quest '$questName' completed successfully!")
            messages.drop(1).reversed().forEach { LogPanel.append(it) }

            terminateQuest()
        } ?: failedQuestPattern.mapNotNull { it.matchEntire(messages.first())?.groupValues }.firstOrNull()?.let { (_, questName) ->
            LogPanel.append("Quest '$questName' failed.")

            terminateQuest()
        } ?: prematureQuestStart.mapNotNull { it.matchEntire(messages.first())?.groupValues }.firstOrNull()?.let { (_, availableTime) ->
            availableTime.toTimeTry()?.let { time ->
                lastQuestAccepted = System.currentTimeMillis() - (60*60*1000 - LocalTime.now().until(time.plusMinutes(1), ChronoUnit.MILLIS))
                QuestPanel.nextQuestTime = df.format(time)
                LogPanel.append("Can't accept a new quest until ${df.format(time)}.")
            } ?: LogPanel.append(messages.first())
        }

        prefs.flush()
    }

    private fun terminateQuest() {
        currentQuest = null
        prefs.remove("currentQuest")
        questEnds = 0
        questLength = 1
        prefs.putLong("questEnds", questEnds)
        prefs.putLong("questLength", questLength)
    }
}