package xyz.jmullin.daringhero.bot

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.*
import xyz.jmullin.daringhero.bot.model.*
import xyz.jmullin.daringhero.entity.*
import xyz.jmullin.daringhero.notify
import xyz.jmullin.daringhero.nullIfEmpty
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter
import java.net.SocketTimeoutException
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit


object Api {

    val logDf = DateTimeFormatter.ofPattern("MMddYYhh")

    val ApiKey = "1fb09992-e417-4364-8fd9-df9a5162b0db"
    val BaseUrl = "http://nerdquest.nerderylabs.com:1337"
    val Mapper = ObjectMapper().registerModule(KotlinModule())!!

    val client = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            write("log/${OffsetDateTime.now().format(logDf)}.json", message, true)
        }).apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        .connectTimeout(3, TimeUnit.SECONDS)
        .readTimeout(3, TimeUnit.SECONDS)
        .writeTimeout(3, TimeUnit.SECONDS)
        .build()!!

    val retrofit = Retrofit.Builder()
        .client(client)
        .addConverterFactory(JacksonConverterFactory.create(Mapper))
        .baseUrl(BaseUrl).build()!!
    val gameService = retrofit.create(ServiceInterface::class.java)!!

    var lastDud = ""
    var dudCount = 0

    var timeouts = 0

    val expectedMessagePatterns = LogPanel.hiddenMessagePatterns + LogPanel.pointsMessagePatterns +
        listOf(
            ".*a(qc|cq)uired a new item.*",
            "You found a bonus item!.*",
            "Thou hast discovered a hidden treasure.*",
            "Thou hast discovered a grand treasure.*",
            "Upon completion of thy arduous task, ye are rewarded with a.*",
            "You used <[^>]+>.*",
            "Valiantly thy hero applieth",
            "You afflicted \\w+.*",
            "You broke rule.*",
            "Thou hast angered the gods by trespassing.*",
            "You got BURNED.*",
            ".*ribbit.*"
        ).map { it.toRegex() }

    fun search() {
        request("points", gameService.postPoints(ApiKey), { (messages, item, points, effects, badges) ->
            StatusIndicator.success("points", "Posted to points successfully.")
            item?.let { item ->
                InventoryPanel.addToInventory(item)
                val name = if(item.name == "Co-president's Coin") {

                } else item.name
                write("data/inventory.txt", """{ "item": "$name", "id": "${item.id}" },""", true)
            }
            PlayerPanel.updateMyScore(points, effects, badges)
            messages.forEach { m ->
                LogPanel.append(m)

                if(expectedMessagePatterns.filter { m.matches(it) }.isEmpty()) {
                    notify("Unexpected message!", m)
                }
            }
        })
    }

    fun getLeaderboard(page: Int, onSuccess: (List<ProfileSummary>) -> Unit, onFailure: (Int) -> Unit) {
        request("leaderboard", if(page == 0) gameService.leaderboardTop(ApiKey) else gameService.leaderboard(ApiKey, page),
            onSuccess, onFailure)
    }

    fun use(itemName: String, itemId: UUID, target: String): Boolean {
        var success = false
        request("use", gameService.useItem(ApiKey, itemId, target), { (Messages, Points) ->
            StatusIndicator.success("use", "Used $itemName successfully.")
            InventoryPanel.removeFromInventory(itemName, itemId)
            Api.write("data/itemUsage.txt", "Used $itemName ($itemId) on $target.", true)
            PlayerPanel.updateMyScore(Points)
            Messages.forEach { m -> LogPanel.append(m) }
            success = true
        }, { errorCode ->
            if(Gdx.input.isKeyPressed(Input.Keys.MINUS)) {
                Api.write("data/itemUsage.txt", "Ditched dud $itemName ($itemId).", true)
                LogPanel.append("Ditched a dud $itemName.")
                write("data/duds.txt", "$itemName - $itemId", true)
                InventoryPanel.removeFromInventory(itemName, itemId)
            }

            if(errorCode == 400 || errorCode == 401) {
                if(itemId.toString() == lastDud) dudCount += 1
                else dudCount = 0
                lastDud = itemId.toString()

                Controller.lastItemUse = System.currentTimeMillis() - Controller.itemDelay + 1700
                Controller.prefs.putLong("lastItemUse", Controller.lastItemUse)
                Controller.prefs.flush()

                if(dudCount > 4) {
                    Api.write("data/itemUsage.txt", "Ditched dud $itemName ($itemId).", true)
                    LogPanel.append("Ditched a dud $itemName.")
                    write("data/duds.txt", "$itemName - $itemId", true)
                    InventoryPanel.removeFromInventory(itemName, itemId)
                }
            }
        })
        return success
    }

    fun useAbility(abilityName: String, target: String): Boolean {
        var success = false
        request("ability", gameService.activateAbility(ApiKey, abilityName, target), { (Messages, Points) ->
            StatusIndicator.success("ability", "Used $abilityName successfully.")
            Api.write("data/abilityUsage.txt", "Used $abilityName on $target.", true)
            PlayerPanel.updateMyScore(Points)
            Messages.forEach { m -> LogPanel.append(m) }
            success = true
        }, { _ ->

        })
        return success
    }

    fun switchClass(className: String): Boolean {
        var success = false
        request("class", gameService.switchClass(ApiKey, className), { (Messages) ->
            AbilityPanel.switchClass(className)
            StatusIndicator.success("class", "Switched successfully to $className.")
            Messages.forEach { m -> LogPanel.append(m) }
            success = true
        }, { _ ->
            AbilityPanel.retryClass = className
            AbilityPanel.switchRetry = 1f
        })
        return success
    }

    fun levelUp(className: String): Boolean {
        var success = false
        request("class", gameService.levelUp(ApiKey, className), { (Messages) ->
            AbilityPanel.switchClass(className)
            Controller.classLevels += className to (Controller.classLevels[className] ?: 0 + 1)
            Controller.prefs.putString("classLevels", Api.Mapper.writeValueAsString(Controller.classLevels))
            Controller.prefs.flush()
            StatusIndicator.success("class", "Successfully leveled up $className.")
            Messages.forEach { m -> LogPanel.append(m) }
            success = true
        }, { _ ->
            AbilityPanel.retryLevelClass = className
            AbilityPanel.levelRetry = 1f
        })
        return success
    }

    fun quest(): Boolean {
        var success = false
        request("quest", gameService.fetchRequest(ApiKey), { (Messages) ->
            StatusIndicator.success("quest", "Checked on quests successfully.")
            Controller.handleQuestMessages(Messages)
            success = true
        }, { _ ->

        })
        return success
    }

    fun getSelfEffects() {
        var success = false
        request("status", gameService.individualEffects(ApiKey, "jmullin"), { info ->
            StatusIndicator.success("effect", "Fetch effects on self successfully.")
//            EffectsPanel.add(info)
        })
    }

    fun getEffects(target: String = "jmullin") {
        var success = false
        request("effect", gameService.individualEffects(ApiKey, target), { info ->
            StatusIndicator.success("effect", "Fetch effects on $target successfully.")
            EffectsPanel.add(info)
        })
    }

    interface ServiceInterface {
        @POST("items/use/{item}")
        fun useItem(@Header("apikey") apiKey: String, @Path("item") item: UUID, @Query("target") target: String): Call<UseEvent>

        @POST("ability/{name}")
        fun activateAbility(@Header("apikey") apiKey: String, @Path("name") name: String, @Query("target") target: String): Call<UseEvent>

        @POST("class/{className}")
        fun switchClass(@Header("apikey") apiKey: String, @Path("className") name: String): Call<GenericEvent>

        @POST("class/{className}/levelup")
        fun levelUp(@Header("apikey") apiKey: String, @Path("className") name: String): Call<GenericEvent>

        @POST("points")
        fun postPoints(@Header("apikey") apiKey: String): Call<Event>

        @POST("quest")
        fun fetchRequest(@Header("apikey") apiKey: String): Call<GenericEvent>

        @GET("effects")
        fun globalEffects(@Header("apikey") apiKey: String): Call<List<EffectInfo>>

        @GET("effects/{player}")
        fun individualEffects(@Header("apikey") apiKey: String, @Path("player") player: String): Call<List<EffectInfo>>

        @Headers("Accept: application/json")
        @GET("/")
        fun leaderboardTop(@Header("apikey") apiKey: String): Call<List<ProfileSummary>>

        @Headers("Accept: application/json")
        @GET("/")
        fun leaderboard(@Header("apikey") apiKey: String, @Query("page") page: Int): Call<List<ProfileSummary>>
    }

    private fun <T> request(statusId: String, request: Call<T>, onSuccess: (T) -> Unit, onFailure: (Int) -> Unit = {}) {
        StatusIndicator.update(statusId, Color.YELLOW, false)
        val response = try {
            request.execute()
        } catch (e: SocketTimeoutException) {
            onFailure(999)
            return
        }
        if(response.isSuccessful) {
            StatusIndicator.update(statusId, Color.GREEN, true)
            onSuccess(response.body()!!)
        } else {
            StatusIndicator.update(statusId, Color.RED, true)
            response.errorBody()?.string()?.nullIfEmpty()?.let {
                StatusIndicator.error(statusId, it)
            } ?: StatusIndicator.error(statusId, "Received error ${response.code()} with no response body.")
            onFailure(response.code())
            write("data/errors.txt", response.errorBody()?.string() ?: "No error body.", true)
        }
    }

    fun write(file: String, output: String, append: Boolean = false) {
        val writer = PrintWriter(FileOutputStream(File(file), append))
        writer.println(output)
        writer.flush()
        writer.close()
    }
}
