package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*

class ToggleButton(val col: Color, val label: String, val active: () -> Boolean, val toggle: () -> Unit) : Entity2D() {
    var hoverGlow = 0f
    var clickGlow = 0f
    var selectedGlow = 0f

    var hoverTime = 0f

    override fun update(delta: Float) {
        if (containsPoint(mouseV())) {
            hoverGlow += (0.15f - hoverGlow) / 5f
        } else {
            hoverGlow -= hoverGlow / 5f
        }

        if (active()) {
            selectedGlow += (0.25f - selectedGlow) / 5f
        } else {
            selectedGlow -= selectedGlow / 5f
        }

        clickGlow -= clickGlow / 7f

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        val glow =
            FloatMath.clamp(if (!active()) {
                0.25f + hoverGlow
            } else {
                0.4f + hoverGlow
            } + clickGlow + selectedGlow, 0f, 1f)

        val (a, b, c) = if (active()) listOf(1f, 1f, 0.65f) else listOf(1f, 1f, 0.8f)

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            Draw.fill.color = col.cpy() * glow * a
            sprite(Draw.fill, position + V2(4f), size - V2(8f))

            Draw.fill.color = col.cpy() * glow * c
            sprite(Draw.fill, position + V2(8f), size - V2(16f))

            string(label, position + size/2f, Assets.prototype14, V2(0f))
        }

        super.render(stage)
    }

    override fun touchUp(v: Vector2, pointer: Int, button: Int): Boolean {
        if (button == 0) {
            toggle()
        }

        return super.touchDown(v, pointer, button)
    }
}
