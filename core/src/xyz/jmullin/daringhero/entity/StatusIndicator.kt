package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.shader.HeroShaders.cooldownShader
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.entity.EntityContainer2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*
import java.time.LocalTime

class StatusIndicator(val label: String, val id: String, val progress: (() -> Float)? = null) : Entity2D() {
    companion object {
        var indicators = mapOf<String, StatusIndicator>()

        fun update(id: String, color: Color, decay: Boolean) {
            indicators[id]?.let {
                if(decay) {
                    it.pulse = 1f
                } else {
                    it.pulse = 0f
                }
                it.decay = decay
                it.led = color
            }
        }

        fun success(id: String, message: String) {
            indicators[id]?.logPanel?.append(message, StatusLogPanel.MessageType.Success)
        }

        fun status(id: String, message: String) {
            indicators[id]?.logPanel?.append(message, StatusLogPanel.MessageType.Status)
        }

        fun error(id: String, message: String) {
            indicators[id]?.logPanel?.append(message, StatusLogPanel.MessageType.Error)
        }

        fun closeAll() {
            indicators.forEach { it.value.logPanel.extend = false }
        }
    }

    val logPanel = StatusLogPanel(id).apply {
        size.set(gameW().toFloat()-12f, LogPanel.height)
        position.set(V2(6f, -LogPanel.height))
    }

    var pulse = 0f
    var clickGlow = 0f
    var decay = false

    var led = Color.BLACK!!

    override fun create(container: EntityContainer2D) {
        indicators += id to this
        depth = -100
        parent?.add(logPanel)

        super.create(container)
    }

    override fun update(delta: Float) {
        if (decay) {
            pulse -= pulse / 15f
        } else {
            pulse += (1f - pulse) / 3f
        }

        clickGlow -= clickGlow/5f

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        val (a, b, c) = listOf(1f, 1f, 0.65f)

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 1f, size - 2f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 2f, size - 4f)

            Draw.fill.color = C(1f).alpha(0.75f*clickGlow)
            sprite(Draw.fill, position, size)

            Draw.fill.color = led.cpy() * pulse * a
            sprite(Draw.fill, position + V2(2f), size - V2(4f))

            Draw.fill.color = led.cpy() * pulse * c
            sprite(Draw.fill, position + V2(4f), size - V2(8f))

            progress?.let {
                Shaders.switch(cooldownShader, this)

                cooldownShader.program?.setUniformf("progress", 1f - it().clamp(0f, 1f))
                cooldownShader.program?.setUniformf("center", position + size/2f)

                Draw.fill.color = Color.WHITE.alpha(0.35f)
                sprite(Draw.fill, position + V2(4f), size - V2(8f))

                Shaders.switch(Shaders.default, this)
            }


            Assets.petMe12.color = Color.WHITE
            string(label, position + size / 2f - V2(0f, 1f), Assets.prototype14, V2(0f))
        }

        super.render(stage)
    }

    override fun touchDown(v: Vector2, pointer: Int, button: Int): Boolean {
        if(logPanel.extend || button == 1) {
            closeAll()
        } else {
            closeAll()
            logPanel.extend = true
        }
        clickGlow = 1f

        return super.touchDown(v, pointer, button)
    }

}

class StatusLogPanel(name: String) : Entity2D() {
    val MaxMessages = 150

    enum class MessageType(val col: Color) {
        Success(C(0.6f, 1f, 0.6f)),
        Status(Color.WHITE),
        Error(C(1f, 0.6f, 0.6f))
    }

    var messages = listOf("${name.capitalize()} ready to begin making requests." to MessageType.Status)

    var scroll = 0f
    var scrollV = 0f

    var extend = false

    override fun render(stage: RenderStage) {
        if(position.y + size.y <= 0f) return

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            flush()

            val scissors = Rectangle()
            val clipBounds = Rect(position + 6f, size - 12f - V2(0f, 3f))
            ScissorStack.calculateScissors(layer()?.camera, transformMatrix, clipBounds, scissors)
            ScissorStack.pushScissors(scissors)

            messages.withIndex().forEach { (i, message) ->
                Assets.fontTiny.color = message.second.col
                string(message.first, position + V2(8f, height - 8f - i * 15f + scroll), Assets.fontTiny)
            }

            flush()
            ScissorStack.popScissors();
        }
    }

    override fun update(delta: Float) {
        scroll += scrollV
        scrollV *= 0.75f
        scroll = scroll.clamp(0f, Math.max(0, messages.size - ((height-18f) / 15f).toInt()) * 15f)

        if(extend) {
            position.y += (-6f - position.y) / 10f
        } else {
            position.y += (-LogPanel.height - position.y) / 10f
        }

        super.update(delta)
    }

    fun append(rawMessage: String, type: MessageType) {
        messages = listOf("${LocalTime.now()}: $rawMessage" to type) + messages
        messages = messages.take(MaxMessages)
    }

    override fun scrolled(amount: Int): Boolean {
        scrollV += amount*1.5f

        return super.scrolled(amount)
    }

    override fun touchDown(v: Vector2, pointer: Int, button: Int): Boolean {
        if(LogPanel.containsPoint(mouseV())) StatusIndicator.closeAll()

        return super.touchDown(v, pointer, button)
    }
}