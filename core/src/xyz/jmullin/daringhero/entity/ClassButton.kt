package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import xyz.jmullin.daringhero.Icons
import xyz.jmullin.daringhero.bot.Api
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*

class ClassButton(val className: String) : Entity2D() {
    var hoverGlow = 0f
    var clickGlow = 0f

    val active: Boolean get() = Controller.currentClass == className
    val icon by lazy { Icons.getIcon(className) }

    val level: Int get() = Controller.classLevels[className] ?: 0

    var led = Color.BLACK!!

    override fun update(delta: Float) {
        if (containsPoint(mouseV()) && !active) {
            hoverGlow += (0.15f - hoverGlow) / 5f
            AbilityPanel.buttons
                .filter { it.ability.classOrigin == className }
                .forEach { it.tempShow = true }
            AbilityPanel.buttons
                .filterNot { it.hidden }
                .forEach { it.tempHide = true }
        } else {
            hoverGlow -= hoverGlow / 5f
        }

        clickGlow -= clickGlow/5f

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        val (a, b, c) = listOf(1f, 1f, 0.65f)

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 1f, size - 2f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 2f, size - 4f)

            Draw.fill.color = C(1f).alpha(0.75f*FloatMath.max(clickGlow, hoverGlow))
            sprite(Draw.fill, position, size)

            Draw.fill.color = led.cpy() * a
            sprite(Draw.fill, position + V2(2f), size - V2(4f))

            Draw.fill.color = led.cpy() * c
            sprite(Draw.fill, position + V2(4f), size - V2(8f))

            icon.setAlpha(if(active) 1f else (0.5f + hoverGlow))
            sprite(icon, position + V2(4f), size - V2(8f))

            if(bounds.contains(mouseV())) {
                TinyTooltip.set(className, position + size/2f)
            }

            (0 until level).forEachIndexed { i, _ ->
                Draw.fill.color = if(active) Color.LIGHT_GRAY else Color.GRAY
                sprite(Draw.fill, position + size - V2(10f, 10f + 6f*i), V2(6f))

                Draw.fill.color = if(active) Color.WHITE else Color.LIGHT_GRAY
                sprite(Draw.fill, position + size - V2(9f, 9f + 6f*i), V2(4f))
            }
        }

        super.render(stage)
    }

    override fun touchDown(v: Vector2, pointer: Int, button: Int): Boolean {
        clickGlow = 1f
        if(className != Controller.currentClass) Api.switchClass(className)

        return super.touchDown(v, pointer, button)
    }

}