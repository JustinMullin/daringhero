package xyz.jmullin.daringhero.entity

import xyz.jmullin.daringhero.bot.Api
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.daringhero.bot.model.*
import xyz.jmullin.daringhero.hours
import xyz.jmullin.daringhero.notify
import xyz.jmullin.daringhero.sliding
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.C
import xyz.jmullin.drifter.extensions.V2
import xyz.jmullin.drifter.extensions.minus
import xyz.jmullin.drifter.extensions.plus
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import java.util.*

object InventoryPanel : Entity2D() {
    var catalog = Catalog(listOf())
    var _inventory = Inventory(listOf())

    val ItemExpiration = 48.hours

    val inventory: Inventory get() {
        val time = System.currentTimeMillis()
//        return _inventory.copy(items = _inventory.items.filter { it.foundAt <= System.currentTimeMillis() - 57600000 })
        return _inventory
    }

    fun unselectItems() {
        if (!itemSelectionSticky) {
            itemButtons.forEach { it.selected = false }
            itemSelected = null
        } else {
            itemSelected = itemSelected?.let { item ->
                inventory.items.find { it.item == item.catalog.name }?.let { id ->
                    ItemSelection(item.catalog, id.id)
                }
            }
        }
    }

    fun categoryOrder(category: String): Char {
        val order = listOf(
            "speedBoost", "pointBoost", "weapon", "swapper", "itemBox",
            "distraction", "shield", "dispel", "badge", "uncategorized", "badStuff"
        )
        return order.indexOf(category).let { (if(it == -1) order.indexOf("uncategorized") else it) + 'a'.toInt() }.toChar()
    }

    val autoDropItems = listOf<String>()
    val hiddenItems = listOf("Bo Jackson", "Pandora's Box", "Box of Bees", "Leisure Suit", "NPC")

    fun load() {
        val catalogPrefs = Controller.prefs.getString("catalog")
        val inventoryPrefs = Controller.prefs.getString("inventory")

        catalog = if (catalogPrefs.isEmpty()) Catalog(listOf()) else Api.Mapper.readValue(catalogPrefs, Catalog::class.java)
        _inventory = if (inventoryPrefs.isEmpty()) Inventory(listOf()) else Api.Mapper.readValue(inventoryPrefs, Inventory::class.java)

        inventory.items.map { it.item }.distinct().forEach { name ->
            if (catalog.entries.find { it.name == name } == null && !autoDropItems.contains(name)) throw Exception("Missing item in catalog! ($name)")
        }

        LogPanel.append("DaringHero intialized.")
        LogPanel.append("${catalog.entries.size} items catalogued; ${inventory.items.size} items in inventory.")

        populateButtons()
    }

    data class Button(val item: ItemButton?, val ability: AbilityButton?)
    data class ButtonDefinition(val item: CatalogEntry?, val ability: Ability?)

    fun populateButtons() {
        children.forEach(Entity2D::remove)

        val displayCatalog = catalog.entries.filterNot { hiddenItems.contains(it.name) }

        val gridRows = Math.max(Math.ceil(Math.pow(displayCatalog.size.toDouble(), 0.5)).toInt(), 1)
        val gridColumns = Math.max(Math.ceil(displayCatalog.size.toDouble() / gridRows.toFloat()).toInt(), 1)

        val dX = (width-8f) / gridColumns.toFloat()
        val dY = (height-8f) / gridRows.toFloat()

        val itemButtons = displayCatalog.sortedBy { (category, _, name, rarity) ->
            categoryOrder(category ?: "").toString() + "-" + rarity + "-" + name
        }.map { ButtonDefinition(it, null) }

        val rows = (itemButtons).sliding(gridColumns)
            .map { it.withIndex() }.withIndex()

        rows.forEach { (yI, row) ->
            row.forEach { (xI, button) ->
                if(button.item != null) {
                    val count = inventory.items.count { it.item == button.item.name }
                    val itemButton = ItemButton(button.item, count, position + V2(4f + dX * xI, 4f + dY * yI), V2(dX, dY))
                    buttons += Button(itemButton, null)
                    add(itemButton)
                }
            }
        }
    }

    fun addToCatalog(item: Item) {
        catalog = Catalog(catalog.entries + CatalogEntry(null, null, item.name, item.rarity, item.description))
        Controller.prefs.putString("catalog", Api.Mapper.writeValueAsString(catalog))
        Controller.prefs.flush()
        populateButtons()
    }

    fun addToInventory(item: Item) {
        _inventory = Inventory(inventory.items + InventoryEntry(item.name, item.id))
        Controller.prefs.putString("inventory", Api.Mapper.writeValueAsString(inventory))
        Controller.prefs.flush()
        if (catalog.entries.find { it.name == item.name } == null && !autoDropItems.contains(item.name)) {
            notify("${item.name} added to catalog!", "${item.name} [${item.rarityText}]: ${item.description}")
            addToCatalog(item)
        }
        if(item.rarity > 4) {
            notify("${item.rarityText.capitalize()} ${item.name} collected!", "${item.name} [${item.rarityText}]: ${item.description}")
        }
        itemButtons.find { it.item.name == item.name }?.updateCount(inventory.items.count { it.item == item.name })
    }

    fun removeFromInventory(name: String, id: UUID) {
        _inventory = Inventory(inventory.items.filterNot { it.id == id })
        Controller.prefs.putString("inventory", Api.Mapper.writeValueAsString(inventory))
        Controller.prefs.flush()
        itemButtons.find { it.item.name == name }?.updateCount(inventory.items.count { it.item == name })
    }

    var buttons = setOf<Button>()
    val itemButtons: List<ItemButton> get() = buttons.mapNotNull { it.item }

    data class ItemSelection(val catalog: CatalogEntry, val id: UUID)

    var itemSelected: ItemSelection? = null
    var itemSelectionSticky = false

    fun select(button: ItemButton, sticky: Boolean = false) {
        itemButtons.forEach { it.selected = false }
        button.selected = true
        itemSelectionSticky = sticky
        itemSelected = ItemSelection(button.item, inventory.items.find { it.item == button.item.name }?.id!!)
    }

    override fun render(stage: RenderStage) {
        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)
        }

        super.render(stage)
    }
}
