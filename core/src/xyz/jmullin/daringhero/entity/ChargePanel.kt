package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.extensions.FloatMath.clamp
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import xyz.jmullin.drifter.rendering.string

class ChargePanel() : Entity2D() {
    constructor(v: Vector2, _size: Vector2): this() {
        position.set(v)
        size.set(_size)
    }

    fun blue(i: Float) = C(i * 0.4f, i * 0.4f, i*0.45f)
    var lastProgress = -1f
    var readyGlow = 0f

    companion object {
        var tick = 0f
    }

    override fun update(delta: Float) {
        tick += delta * 0.5f
        if(tick > 1f) tick -= 1f

        readyGlow *= 0.95f

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        val progress = clamp(Controller.timeSinceLastItemUse / Controller.itemDelay.toFloat(), 0f, 1f)
        if(progress >= 1f && lastProgress < 1f && lastProgress != -1f) {
            readyGlow = 1f
        }
        lastProgress = progress

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            Draw.fill.color = blue(0.85f + 0.1f*readyGlow)
            sprite(Draw.fill, position + 4f, V2((width - 8f)*progress, height - 8f))

            Draw.fill.color = blue(0.3f + 0.6f*readyGlow)
            sprite(Draw.fill, position + 6f, V2((width - 8f)*progress - 4f, height - 12f))

            if(progress < 1f) {
                Assets.prototype14.color = C(C(0.8f), (1f-tick)*2f)
                string("Charging...", position + size/2f, Assets.prototype14, V2(0f))
            }
            Assets.prototype14.color = C(0.8f)
            if(progress >= 1f) string("Item activation ready!", position + size/2f, Assets.prototype14, V2(0f))
        }

        super.render(stage)
    }
}
