package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.bot.model.Badge
import xyz.jmullin.daringhero.bot.model.ProfileSummary
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import java.math.BigInteger

object PlayerPanel : Entity2D() {
    var stickySelection = false

    var myScore = BigInteger("0")
    var myEffects = listOf<String>()
    var myBadges = listOf<Badge>()
    var myTitle = ""

    val label = Label("", LabelStyle(Assets.fontMiniscule, Color.YELLOW)).apply {
        setWrap(true)
    }

    var targetSelected: String? = null

    fun updateMyScore(points: BigInteger) {
        myScore = points
        playerButtons.find { it.player.playerName == "jmullin" }?.let {
            it.player = it.player.copy(points = points)
        }
    }

    fun toastPoints(gained: String) {
        playerButtons.find { it.player.playerName == "jmullin" }?.let { me ->
            me.toastPoints(gained)
        }
    }

    fun updateMyScore(points: BigInteger, effects: List<String>, badges: List<Badge>) {
        myScore = points
        myEffects = effects
        myBadges = badges
        updateMyScore(points)
    }

    var playerButtons = setOf<PlayerButton>()

    fun updateButtons(players: List<IndexedValue<ProfileSummary>>) = synchronized(this) {
        val cardHeight = (gameH() - 8f) / players.size.toFloat()

        val oldButtons = children.flatMap {
            when (it) {
                is PlayerButton -> listOf(it.player.playerName to it)
                else -> listOf()
            }
        }.toMap()

        children.forEach(Entity2D::remove)

        playerButtons = setOf()

        players.withIndex().forEach { (i, playerWithIndex) ->
            val rank = playerWithIndex.index + 1
            val player = playerWithIndex.value
            val preexisting = oldButtons[player.playerName]

            val playerButton = PlayerButton (rank, player, V2(8f, 0f) + V2(x, y+height-cardHeight * (i+1)), V2(width - 8f, cardHeight), preexisting)

            if (targetSelected == player.playerName) {
                playerButton.selected = true
                playerButton.selectedGlow = playerButton.SelectedMaxGlow
            }

            add(playerButton)
            playerButtons += playerButton
        }
    }

    fun select(button: PlayerButton, sticky: Boolean = false) {
        playerButtons.forEach { it.selected = false }
        button.selected = true
        PlayerPanel.stickySelection = sticky
        targetSelected = button.player.playerName
    }

    fun unselect() {
        if (!stickySelection) {
            playerButtons.forEach { it.selected = false }
            targetSelected = null
        }
    }

    override fun render(stage: RenderStage) {
        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)
        }

        super.render(stage)
    }
}