package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.daringhero.nullIfEmpty
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import xyz.jmullin.drifter.rendering.string
import java.time.LocalTime
import java.time.ZoneId
import java.time.Instant
import java.time.LocalDate



object QuestPanel : Entity2D() {

    var nextQuestTime = ""

    override fun render(stage: RenderStage) {
        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            Controller.currentQuest?.let { quest ->
                val timeLeft = Controller.timeRemainingInQuest / Controller.questLength.toFloat()

                if(timeLeft <= 0) {
                    Draw.fill.color = C(0.05f, 0.05f, 0.25f)
                    sprite(Draw.fill, position + 4f, size - 8f)
                } else {
                    Draw.fill.color = C(0.25f)
                    sprite(Draw.fill, position + 4f, (size - 8f) * V2(timeLeft, 1f))
                }

                Assets.fontMiniscule.color = Color.WHITE
                string(quest, position + V2(8f, height/2f+3f), Assets.fontMiniscule, V2(1f, 0f))
            } ?: {
                val chargeProgress = 1f - FloatMath.clamp(QuestButton.timeRemaining / QuestButton.duration.toFloat(), 0f, 1f)
                Draw.fill.color = C(0.15f, 0.15f, 0.15f)
                sprite(Draw.fill, position + 6f, (size - 12f) * V2(chargeProgress, 1f))

                Assets.fontMiniscule.color = Color.WHITE
                if(chargeProgress < 1f) {
                    nextQuestTime.nullIfEmpty()?.let {
                        string("Next quest available at $it.", position + V2(8f, height/2f+3f), Assets.fontMiniscule, V2(1f, 0f))
                    } ?: {
                        if(Controller.lastQuestAccepted > 0) {
                            val lastAcceptedTime = Instant.ofEpochMilli(Controller.lastQuestAccepted).atZone(ZoneId.systemDefault()).toLocalTime()
                            val nextTime = lastAcceptedTime.plusHours(1)
                            string("Next quest available at ${Controller.df.format(nextTime)}.", position + V2(8f, height/2f+3f), Assets.fontMiniscule, V2(1f, 0f))
                        }
                    }()
                } else {
                    Draw.fill.color = C(0.05f, 0.25f, 0.05f)
                    sprite(Draw.fill, position + 4f, size - 8f)
                    string("New quest available!", position + V2(8f, height/2f+3f), Assets.fontMiniscule, V2(1f, 0f))
                }
            }()
        }

        super.render(stage)
    }

}
