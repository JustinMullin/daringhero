package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.bot.Api
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*

object LevelUpButton : Entity2D() {
    var hoverGlow = 0f
    var clickGlow = 0f

    val experienceNeeded: Long get() = when(Controller.classLevels[Controller.currentClass] ?: 0) {
        0 -> 5000
        1 -> 50000
        2 -> 250000
        3 -> 500000
        4 -> 1000000
        else -> Long.MAX_VALUE
    }
    val active: Boolean get() = PlayerPanel.myScore.toLong() >= experienceNeeded

    var led = Color.BLACK!!

    override fun update(delta: Float) {
        if (containsPoint(mouseV()) && active) {
            hoverGlow += (0.15f - hoverGlow) / 5f
        } else {
            hoverGlow -= hoverGlow / 5f
        }

        clickGlow -= clickGlow/5f

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        val (a, b, c) = listOf(1f, 1f, 0.65f)

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 1f, size - 2f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 2f, size - 4f)

            Draw.fill.color = C(1f).alpha(0.75f*FloatMath.max(clickGlow, hoverGlow))
            sprite(Draw.fill, position, size)

            Draw.fill.color = led.cpy() * a
            sprite(Draw.fill, position + V2(2f), size - V2(4f))

            Draw.fill.color = led.cpy() * c
            sprite(Draw.fill, position + V2(4f), size - V2(8f))

            Draw.fill.color = Color.GREEN.alpha(if(active) 0.15f else (0.05f + hoverGlow))
            sprite(Draw.fill, position + V2(2f), size - V2(4f))

            Assets.fontTiny.color = if(active) Color.WHITE else Color.GRAY
            string("Level Up", position + size/2f, Assets.fontTiny, V2(0f))
        }

        super.render(stage)
    }

    override fun touchDown(v: Vector2, pointer: Int, button: Int): Boolean {
        clickGlow = 1f
        Api.levelUp(Controller.currentClass)

        return super.touchDown(v, pointer, button)
    }

}