package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.bot.model.EffectInfo
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import xyz.jmullin.drifter.rendering.string
import java.time.LocalTime

object EffectsPanel : Entity2D() {
    val MaxMessages = 150

    var playerName = ""

    enum class EffectType(val col: Color) {
        Unclassified(Color.WHITE),
        Positive(C(0.6f, 1f, 0.6f)),
        Negative(C(1f, 0.6f, 0.6f))
    }

    val initialMessages = listOf("Effects log for $playerName" to EffectType.Unclassified)

    var messages = initialMessages

    var scroll = 0f
    var scrollV = 0f

    var extend = false

    override fun render(stage: RenderStage) {
        if(position.y + size.y <= 0f) return

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            flush()

            val scissors = Rectangle()
            val clipBounds = Rect(position + 6f, size - 12f - V2(0f, 3f))
            ScissorStack.calculateScissors(layer()?.camera, transformMatrix, clipBounds, scissors)
            ScissorStack.pushScissors(scissors)

            messages.withIndex().forEach { (i, message) ->
                Assets.fontTiny.color = message.second.col
                string(message.first, position + V2(8f, height - 8f - i * 15f + scroll), Assets.fontTiny)
            }

            flush()
            ScissorStack.popScissors();
        }
    }

    override fun update(delta: Float) {
        scroll += scrollV
        scrollV *= 0.75f
        scroll = scroll.clamp(0f, Math.max(0, messages.size - ((height-18f) / 15f).toInt()) * 15f)

        if(extend) {
            position.x += ((PlayerPanel.x + 6f) - position.y) / 10f
        } else {
            position.x += (gameW() - position.x) / 10f
        }

        if(position.x >= gameW() - 10f) reset()

        super.update(delta)
    }

    fun reset() {
        messages = initialMessages
    }

    fun add(effectInfo: List<EffectInfo>) {
        
    }

    fun append(rawMessage: String, type: EffectType) {
        messages = listOf("${LocalTime.now()}: $rawMessage" to type) + messages
        messages = messages.take(MaxMessages)
    }

    override fun scrolled(amount: Int): Boolean {
        if(LogPanel.containsPoint(mouseV())) scrollV += amount*1.5f

        return super.scrolled(amount)
    }

    override fun touchDown(v: Vector2, pointer: Int, button: Int): Boolean {
        if(LogPanel.containsPoint(mouseV())) StatusIndicator.closeAll()

        return super.touchDown(v, pointer, button)
    }
}