package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.Icons
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.daringhero.bot.model.ProfileSummary
import xyz.jmullin.daringhero.sliding
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.entity.EntityContainer2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import xyz.jmullin.drifter.rendering.string
import java.text.DecimalFormat

class PlayerButton(val rank: Int, var player: ProfileSummary, v: Vector2, _size: Vector2, val preexisting: PlayerButton? = null) : Entity2D() {
    init {
        position.set(v - 4f)
        size.set(_size)
    }

    val avatarSprite by lazy { Icons.getAvatar(player.playerName, player.avatarUrl) }
    val classSprite by lazy { player.className?.let { Icons.getIcon(it) } }

    val label = Label("", Label.LabelStyle(Assets.fontMiniscule, Color.WHITE)).apply {
        setWrap(true)
    }

    val SelectedMaxGlow = 0.25f

    var selected = false
    var pulse = 0f

    var toastAlpha = 0f
    var toastMessage = ""

    var hoverGlow = 0f
    var clickGlow = 0f
    var selectedGlow = 0f

    val layout = GlyphLayout()

    val df = DecimalFormat("0.#")

    fun toastPoints(message: String) {
        toastAlpha = 1f
        toastMessage = message
    }

    override fun create(container: EntityContainer2D) {
        preexisting?.let { p ->
            selected = p.selected
            pulse = p.pulse
            hoverGlow = p.hoverGlow
            clickGlow = p.clickGlow
            selectedGlow = p.selectedGlow
            toastAlpha = p.toastAlpha
            toastMessage = p.toastMessage
        }

        super.create(container)
    }

    override fun update(delta: Float) {
        pulse += delta
        toastAlpha *= 0.98f

        if (containsPoint(mouseV())) {
            hoverGlow += (0.15f - hoverGlow) / 5f
        } else {
            hoverGlow -= hoverGlow / 5f
        }

        if (selected) {
            selectedGlow += (0.25f - selectedGlow) / 5f
        } else {
            selectedGlow -= selectedGlow / 5f
        }

        clickGlow -= clickGlow / 7f

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        synchronized(this) {
            if (parent != null) {
                val selectGlow = if (PlayerPanel.stickySelection) selectedGlow * (0.75f + (pulse * 2f).cos() * 0.5f) else selectedGlow

                val glow = FloatMath.clamp(0.2f + hoverGlow + clickGlow + selectGlow, 0f, 1f)

                val (a, b, c) = if (selected) listOf(1f, 1f, 0.65f) else listOf(1f, 0.6f, 0.8f)

                stage.draw {
                    Draw.fill.color = C(1f) * glow * a
                    sprite(Draw.fill, position, size)

                    Draw.fill.color = C(1f) * glow * b
                    sprite(Draw.fill, position + V2(width - 4f, 0), V2(4, height))
                    sprite(Draw.fill, position + V2(0, height - 4f), V2(width, 4f))

                    Draw.fill.color = C(1f) * glow * c
                    sprite(Draw.fill, position + V2(4f), size - V2(8f))

                    val avatarSize = height - 32f
                    val classSize = 24f

                    sprite(avatarSprite, position + 8f, V2(avatarSize))

                    val classOrigin = position + V2(avatarSize - 4f, avatarSize + 2f)

                    classSprite?.let { cSpr ->
                        Draw.fill.color = Color.DARK_GRAY
                        sprite(Draw.fill, classOrigin - 1f, V2(classSize + 2f))
                        cSpr.setAlpha(1f)
                        sprite(cSpr, classOrigin + 1f, V2(classSize - 2f))

                        if(Rect(classOrigin, V2(classSize)).contains(mouseV())) {
                            player.className?.let {
                                TinyTooltip.set(it, classOrigin + classSize/2f)
                            }
                        }

                        val level = player.classLevel ?: 0
                        (0 until level).toList().sliding(Math.max(2, (level / 2f).ceil().toInt())).forEachIndexed { j, row ->
                            row.forEachIndexed { i, _ ->
                                Draw.fill.color = Color.LIGHT_GRAY
                                sprite(Draw.fill, classOrigin + V2(0f, classSize) - V2(10f + i * 8f, 6f + 8f*j), V2(6f))

                                Draw.fill.color = Color.WHITE
                                sprite(Draw.fill, classOrigin + V2(0f, classSize) - V2(10f + i * 8f, 6f + 8f*j) + V2(1f), V2(4f))
                            }
                        }
                    }

                    Assets.fontSmall.color = if (player.playerName == "jmullin") Color.GREEN else Color.WHITE
                    string(player.playerName, position + V2(24f + avatarSize, height - 8f), Assets.fontSmall, V2(1, -1))
                    if(player.playerName == "jmullin") {
                        layout.setText(Assets.fontSmall, player.playerName)
                        val (c, fmt) = (toastMessage.toDoubleTry()?.let {
                            val formatted = df.format(it)
                            if(it > 0) Color.GREEN to "+$formatted"
                            else if(it < 0) Color.RED to formatted
                            else Color.YELLOW to formatted
                        } ?: Color.BLUE to toastMessage)
                        Assets.fontSmall.color = c.alpha(toastAlpha)
                        string(fmt, position + V2(34f + avatarSize + layout.width, height - 8f), Assets.fontSmall, V2(1, -1))
                    }

                    Assets.fontMiniscule.color = Color.WHITE
                    string(player.title.replace(" - Fears no daily (vote|item) limits", ""), position + V2(24f + avatarSize, height / 2f + 3f), Assets.fontMiniscule, V2(1, 0))
                    Assets.fontSmall.color = Color.CYAN
                    val score = if(player.playerName == "jmullin" && Controller.isRunning) {
                        PlayerPanel.myScore.toString()
                    } else {
                        player.points.toString()
                    }
                    string(score, position + V2(16f + avatarSize, 8f), Assets.fontSmall, V2(1, 1))
                    Assets.fontMiniscule.color = Color.YELLOW

                    val nameWidth = 180f
                    label.setAlignment(Align.right, Align.right)
                    label.color = Color.YELLOW

                    val iconMargin = 2f
                    val iconAreaWidth = (width - nameWidth - avatarSize - 40f)/2f - iconMargin*2f
                    var iconSize = if(player.effects.size > 10) {
                        (height-8f) / 3f - iconMargin * 2f
                    } else {
                        (height-8f) / 2f - iconMargin * 2f
                    }
                    val effectRows = player.effects.sliding((iconAreaWidth/(iconSize+iconMargin)).toInt())
                    drawIcons(effectRows, nameWidth + avatarSize, iconSize, iconMargin)

                    iconSize = if(player.badges.size > 10) {
                        (height-8f) / 3f - iconMargin * 2f
                    } else {
                        (height-8f) / 2f - iconMargin * 2f
                    }
                    val badgeRows = player.badges.map { it.name }.sliding((iconAreaWidth/(iconSize+iconMargin)).toInt())
                    drawIcons(badgeRows, nameWidth + avatarSize + iconAreaWidth + iconMargin*2f, iconSize, iconMargin)
                }

                super.render(stage)
            }
        }
    }

    private fun SpriteBatch.drawIcons(effectRows: List<List<String>>, x: Float, iconSize: Float, iconMargin: Float) {
        effectRows.forEachIndexed { yI, row ->
            row.forEachIndexed { xI, effect ->
                val icon = Icons.getIcon(effect)
                icon.color = Color.WHITE
                val v = position + V2(x, 8f) + V2(iconSize + iconMargin) * V2(xI, yI)
                val s = V2(iconSize)
                sprite(icon, v, s)

                if (Rect(v - iconMargin, s + iconMargin * 2f).contains(mouseV())) {
                    TinyTooltip.set(effect, v + s/2f)
                }
            }
        }
    }

    fun String.toDoubleTry(): Double? = try {
        toDouble()
    } catch (e: NumberFormatException) {
        null
    }

    override fun containsPoint(v: Vector2): Boolean = parent != null && super.containsPoint(v)

    override fun touchDown(v: Vector2, pointer: Int, button: Int): Boolean {

        if (parent != null) {
            if (button == 0) {
                clickGlow = 1f
                PlayerPanel.select(this)
            } else if (button == 2) {
                clickGlow = 1f
                PlayerPanel.select(this, sticky = true)
            } else {
                if (selected) {
                    selected = false
                    clickGlow = 0f
                    PlayerPanel.targetSelected = null
                    PlayerPanel.stickySelection = false
                }
            }
        }

        return super.touchDown(v, pointer, button)
    }
}
