package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.utils.Align
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.Icons
import xyz.jmullin.daringhero.bot.model.CatalogEntry
import xyz.jmullin.daringhero.shader.HeroShaders.cooldownShader
import xyz.jmullin.daringhero.seconds
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.entity.EntityContainer2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*

class ItemButton(val item: CatalogEntry, var count: Int, v: Vector2, _size: Vector2) : Entity2D() {
    init {
        position.set(v)
        size.set(_size)
    }

    val shortName = when(item.name) {
        "" -> "Unknown"
        "Da Da Da Da Daaa Da DAA da da" -> "Da Da DAA"
        else -> item.name
    }

    val label = Label(shortName, LabelStyle(Assets.fontMiniscule, Color.WHITE)).apply {
        setWrap(true)
    }

    var selected = false

    var hoverGlow = 0f
    var clickGlow = 0f
    var selectedGlow = 0f
    var countGlow = 0f

    var hoverTime = 0f

    var autoUse = false

    val duration = item.duration?.seconds
    var lastTimeRemaining = -1f
    var timeRemaining: Long = Long.MAX_VALUE
    val charging: Boolean get() = timeRemaining > 0

    val icon: Sprite by lazy { Icons.getIcon(item.name) }
    val spriteSize by lazy { V2(icon.width, icon.height) }
    val spriteScale by lazy { (size / spriteSize).minComponent * 0.8f }

    var pulse = 0f

    val typeColors = mapOf(
        "weapon" to C(1f, 0.25f, 0.25f),
        "shield" to C(1f, 1f, 0.25f),
        "speedBoost" to C(0.25f, 1f, 0.25f),
        "pointBoost" to C(0.25f, 0.25f, 1f),
        "itemBox" to C(0.8f, 0.25f, 1f),
        "badStuff" to C(0.3f, 0.6f, 0.2f),
        "badge" to C(0.25f, 1f, 1f),
        "swapper" to C(1f, 0.5f, 0.15f),
        "distraction" to C(1f, 0.5f, 0.8f),
        "set" to C(0.25f, 0.75f, 1f),
        "special" to C(0.25f, 1f, 0.7f),
        "dispel" to C(1f, 0.2f, 0.7f)
    )
    val defaultTypeColor = C(0.25f, 1f, 1f)

    val rarityLetter = mapOf(
        1 to "C",
        2 to "U",
        3 to "R",
        4 to "E",
        5 to "L",
        6 to "Q"
    )

    override fun create(container: EntityContainer2D) {


        super.create(container)
    }

    override fun update(delta: Float) {
        pulse += delta

        if (containsPoint(mouseV()) && (duration == null || timeRemaining <= 0)) {
            hoverGlow += (0.15f - hoverGlow) / 5f
            hoverTime += delta
        } else {
            hoverGlow -= hoverGlow / 5f
        }

        if (selected || autoUse) {
            selectedGlow += (0.25f - selectedGlow) / 5f
        } else {
            selectedGlow -= selectedGlow / 5f
        }

        if(containsPoint(mouseV())) {
            TooltipRenderer.hover(item, typeColors[item.category ?: ""] ?: defaultTypeColor)
        }

        clickGlow -= clickGlow / 7f
        countGlow -= countGlow / 30f

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        val selectGlow = if (InventoryPanel.itemSelectionSticky || autoUse) selectedGlow * (0.75f + (pulse * 2f).cos() * 0.5f) else selectedGlow

        val glow =
            FloatMath.clamp(if (count == 0) {
                0.25f
            } else {
                0.4f + hoverGlow
            } + clickGlow + selectGlow + countGlow, 0f, 1f)

        val (a, b, c) = if (selected || autoUse) listOf(1f, 1f, 0.65f) else listOf(0.6f, 1f, 0.8f)

        val color = typeColors[item.category ?: ""] ?: defaultTypeColor

        stage.draw {
            val progress = FloatMath.clamp(duration?.let { d -> timeRemaining / d.toFloat() } ?: 0f, 0f, 1f)
            if(duration != null && progress > 0f) Shaders.switch(cooldownShader, this)
            cooldownShader.program?.setUniformf("progress", progress)
            cooldownShader.program?.setUniformf("center", position + size/2f)

            Draw.fill.color = color.cpy() * glow * a
            sprite(Draw.fill, position, size)

            Draw.fill.color = color.cpy() * glow * b
            sprite(Draw.fill, position + V2(width - 4f, 0), V2(4, height))
            sprite(Draw.fill, position + V2(4f, height - 4f), V2(width - 4f, 4f))

            Draw.fill.color = color.cpy() * glow * c
            sprite(Draw.fill, position + V2(4f), size - V2(8f))

            val spriteOrigin = position + size / 2f - spriteSize * spriteScale * 0.5f

            icon.setAlpha(0.25f)
            sprite(icon, spriteOrigin, spriteSize * spriteScale)

            if(duration != null && progress > 0f) Shaders.switch(Shaders.default, this)

            val textColor = C((1f - countGlow) * (if (count == 0) 0.5f else 1f))

            label.color = textColor
            label.setPosition(x + 8f, y + 8f)
            label.setAlignment(Align.bottom, Align.center)
            label.setSize(width - 16f, height - 16f)
            label.draw(this, 1f)

            Assets.fontTiny.color = textColor
            string(count.toString(), position + V2(0, height) + V2(7f, -6f), Assets.fontTiny, V2(1, -1))

            Assets.fontTiny.color = textColor
            string(rarityLetter[item.rarity] ?: "?", position + V2(width, height) + V2(-7f, -6f), Assets.fontTiny, V2(-1, -1))

        }

        super.render(stage)
    }

    fun updateCount(count: Int) {
        this.count = count
        countGlow = 1f
    }

    override fun touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean {


        return super.touchUp(x, y, pointer, button)
    }

    override fun touchUp(v: Vector2, pointer: Int, button: Int): Boolean {
        if (count > 0 && button == 0 && (duration == null || timeRemaining <= 0)) {
            clickGlow = 1f
            InventoryPanel.select(this)
        } else if (button == 2) {
            clickGlow = 1f
            item.duration?.apply {
                autoUse = true
            } ?: InventoryPanel.select(this, sticky = true)
        } else {
            autoUse = false
            if (selected) {
                selected = false
                clickGlow = 0f
                InventoryPanel.itemSelected = null
                InventoryPanel.itemSelectionSticky = false
            }
        }

        return super.touchDown(v, pointer, button)
    }
}
