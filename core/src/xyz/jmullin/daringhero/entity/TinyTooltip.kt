package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.entity.EntityContainer2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import xyz.jmullin.drifter.rendering.string

object TinyTooltip : Entity2D() {
    var text: String? = null

    val layout = GlyphLayout()

    override fun containsPoint(v: Vector2) = false

    fun set(text: String, v: Vector2) {
        this.text = text
        position.set(v)
    }

    override fun create(container: EntityContainer2D) {
        depth = -1000

        super.create(container)
    }

    override fun render(stage: RenderStage) {
        stage.draw {
            text?.let {
                layout.setText(Assets.fontTiny, text)

                val adjustedV = V2(
                    x.clamp(layout.width/2f + 6f, gameW() - layout.width/2f - 6f),
                    y.clamp(layout.height/2f + 6f, gameH() - layout.height/2f - 6f)
                )

                Draw.fill.color = Color.GRAY
                sprite(Draw.fill, adjustedV - V2(layout.width, layout.height) / 2f - 4f, V2(layout.width, layout.height) + 8f)
                Draw.fill.color = Color.BLACK
                sprite(Draw.fill, adjustedV - V2(layout.width, layout.height) / 2f - 3f, V2(layout.width, layout.height) + 6f)

                Assets.fontTiny.color = Color.WHITE
                string(it, adjustedV, Assets.fontTiny, V2(0f))
            }
        }

        text = null

        super.render(stage)
    }
}