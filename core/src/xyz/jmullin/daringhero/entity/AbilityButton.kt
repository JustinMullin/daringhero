package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.utils.Align
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.Icons
import xyz.jmullin.daringhero.bot.model.Ability
import xyz.jmullin.daringhero.seconds
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*
import xyz.jmullin.daringhero.shader.HeroShaders.cooldownShader

class AbilityButton(val ability: Ability, v: Vector2, _size: Vector2) : Entity2D() {
    init {
        position.set(v)
        size.set(_size)
    }

    val label = Label(ability.name, LabelStyle(Assets.fontMiniscule, Color.WHITE)).apply {
        setWrap(true)
    }

    var selected = false

    var hoverGlow = 0f
    var clickGlow = 0f
    var selectedGlow = 0f
    var chargedGlow = 0f

    var hidden = false
    var tempShow = false
    var tempHide = false

    var autoUse = false

    val duration = ability.duration?.seconds
    var lastTimeRemaining = -1f
    var timeRemaining: Long = Long.MAX_VALUE
    val charging: Boolean get() = timeRemaining > 0

    val icon: Sprite by lazy { Icons.getIcon(ability.name) }
    val spriteSize by lazy { V2(icon.width, icon.height) }
    val spriteScale by lazy { (size / spriteSize).minComponent * 0.8f }

    var pulse = 0f

    override fun update(delta: Float) {
        if(hidden && !tempShow) return

        pulse += delta

        if (containsPoint(mouseV())) {
            hoverGlow += (0.15f - hoverGlow) / 5f
        } else {
            hoverGlow -= hoverGlow / 5f
        }

        if (selected || autoUse) {
            selectedGlow += (0.25f - selectedGlow) / 5f
        } else {
            selectedGlow -= selectedGlow / 5f
        }

        if(containsPoint(mouseV())) {
//            TooltipRenderer.hover(ability, C(0.75f))
        }

        clickGlow -= clickGlow / 7f
        chargedGlow -= chargedGlow / 30f
        if(lastTimeRemaining > 0f && timeRemaining <= 0f) {
            chargedGlow = 1f
        }

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        if(hidden && !tempShow) return
        if(tempHide) {
            tempHide = false
            return
        }

        val selectGlow = if (AbilityPanel.stickySelection || autoUse) selectedGlow * (0.75f + (pulse * 2f).cos() * 0.5f) else selectedGlow

        val glow =
            FloatMath.clamp(if (charging) {
                0.25f
            } else {
                0.4f + hoverGlow
            } + clickGlow + selectGlow + chargedGlow, 0f, 1f)

        val (a, b, c) = if (selected || autoUse) listOf(1f, 1f, 0.65f) else listOf(1f, 1f, 0.8f)

        val color = C(1f)

        stage.draw {
            val progress = FloatMath.clamp(timeRemaining / (duration?.toFloat() ?: 1f), 0f, 1f)
            if(duration != null && progress > 0.01f) Shaders.switch(cooldownShader, this)
            cooldownShader.program?.setUniformf("progress", progress)
            cooldownShader.program?.setUniformf("center", position + size/2f)

            Draw.fill.color = color.cpy() * glow * a
            sprite(Draw.fill, position, size)

            Draw.fill.color = color.cpy() * glow * b
            sprite(Draw.fill, position + V2(width - 4f, 0), V2(4, height))
            sprite(Draw.fill, position + V2(4f, height - 4f), V2(width - 4f, 4f))

            Draw.fill.color = color.cpy() * glow * c
            sprite(Draw.fill, position + V2(4f), size - V2(8f))

            val spriteOrigin = position + size / 2f - spriteSize * spriteScale * 0.5f

            icon.setAlpha(0.5f)
            sprite(icon, spriteOrigin, spriteSize * spriteScale)

            if(duration != null && progress > 0.01f) Shaders.switch(Shaders.default, this)

            val textColor = C(1f)

            label.color = textColor
            label.setPosition(x + 8f, y + 8f)
            label.setAlignment(Align.bottom, Align.center)
            label.setSize(width - 16f, height - 16f)
            label.draw(this, 1f)
        }

        tempShow = false

        super.render(stage)
    }

    override fun touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean {


        return super.touchUp(x, y, pointer, button)
    }

    override fun containsPoint(v: Vector2): Boolean {
        return !hidden && super.containsPoint(v)
    }

    override fun touchUp(v: Vector2, pointer: Int, button: Int): Boolean {
        if(hidden) return false

        if (button == 0 && !charging) {
            clickGlow = 1f
            AbilityPanel.select(this)
        } else if (button == 2) {
            clickGlow = 1f
            ability.duration?.apply {
                autoUse = true
            } ?: AbilityPanel.select(this, sticky = true)
        } else {
            autoUse = false
            if (selected) {
                selected = false
                clickGlow = 0f
            }
        }

        return super.touchDown(v, pointer, button)
    }
}
