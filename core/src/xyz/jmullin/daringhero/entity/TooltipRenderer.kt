package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.bot.model.Ability
import xyz.jmullin.daringhero.bot.model.CatalogEntry
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*

object TooltipRenderer : Entity2D() {
    var hovering = false
    var delay = 0f
    var tipColor = Color.WHITE!!
    var lastItem: CatalogEntry? = null

    var item: CatalogEntry? = null
    var ability: Ability? = null

    fun hover(ability: Ability, color: Color) {
        hovering = true
        this.ability = ability
        item = null
        tipColor = color
    }

    fun hover(item: CatalogEntry, color: Color) {
        hovering = true
        this.item = item
        ability = null
        tipColor = color
    }

    override fun update(delta: Float) {
        if(hovering && item == lastItem) {
            delay += delta
        } else {
            delay = 0f
        }
        lastItem = item

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        // TODO
        //Math.log(target / current) / Math.log(1.02)

        stage.draw {
            if(hovering && delay > 0.5f) {
                Draw.fill.color = C(0.1f)
                sprite(Draw.fill, position, size)

                item?.let { item ->
                    Assets.fontTiny.color = tipColor
                    string(item.name, position + V2(width/2f, height - 8f), Assets.fontTiny, V2(0f, -1f))
                    Assets.fontTiny.color = Color.WHITE
                    stringWrapped(item.description, position + V2(0f, height / 2f+15f), width - 24f, Assets.fontTiny, V2(1f, -1f))
                }
                ability?.let { ability ->
                    Assets.fontTiny.color = tipColor
                    string(ability.name, position + V2(width/2f, height/2f + 8f), Assets.fontTiny, V2(0f, -1f))
//                    Assets.fontTiny.color = Color.WHITE
//                    stringWrapped(item.description, position + V2(0f, height / 2f+15f), width - 24f, Assets.fontTiny, V2(1f, -1f))
                }
            }
        }
        hovering = false

        super.render(stage)
    }
}