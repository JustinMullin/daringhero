package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack
import java.util.UUID

import xyz.jmullin.daringhero.Assets
import xyz.jmullin.daringhero.bot.Api
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.daringhero.bot.model.Ability
import xyz.jmullin.daringhero.bot.model.Item
import xyz.jmullin.daringhero.toIntTry
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.entity.EntityContainer2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import xyz.jmullin.drifter.rendering.string

object LogPanel : Entity2D() {
    val MaxMessages = 150

    val hiddenMessagePatterns = listOf(
        "Kupo!",
        "Kupoth!",
        ".*squeek!.*",
        ".*JENKINS!!!!!.*",
        ".*ribbit.*"
    ).map { it.toRegex() }
    val pointsMessagePatterns = listOf(
        "^jmullin gained (-?\\d+(\\.\\d+)?) points! Total points: (-?\\d+).*",
        ".*Thy hero jmullin hath gained (-?\\d+(\\.\\d+)?) points, and hereby achieveth a grand sum of (-?\\d+) points.*"
    ).map { it.toRegex() }
    val toastMessagePatterns = listOf(
        ".*?(-?\\d+(\\.\\d+)?) points? for jmullin.*"
    ).map { it.toRegex() }
    val bonusItemPattern = listOf(
        "(.*) ?<([0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})> \\| <([^>]+)>(.*)",
        "(.*) ?<([^>]+)> \\| <([0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})>(.*)"
    ).map { it.toRegex() }
    var messages = listOf<String>()
    val abilityGainedPattern = listOf(
        "You have acquired a new ability: <([^>]+)>.*",
        "Thine efforts have proven fruitful, and thou gaineth <([^>]+)>.*"
    ).map { it.toRegex() }
    val levelUpPattern = listOf(
        "You have leveled up to (\\d+) in the most venerable class of <([^>]+)>",
        "Thy hard work doth not be in vain - ye have achieved the grand level of <(\\d+)> in <([^>]+)>"
    ).map { it.toRegex() }

    var fishingMessageDelay = 0f

    var scroll = 0f
    var scrollV = 0f

    override fun create(container: EntityContainer2D) {
//        listOf(
//            ""
//        ).forEach { append(it) }

        super.create(container)
    }

    override fun render(stage: RenderStage) {
        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            flush()

            val scissors = Rectangle()
            val clipBounds = Rect(position + 6f, size - 12f - V2(0f, 3f))
            ScissorStack.calculateScissors(layer()?.camera, transformMatrix, clipBounds, scissors)
            ScissorStack.pushScissors(scissors)

            messages.withIndex().forEach { (i, message) ->
                Assets.fontTiny.color = Color.WHITE
                string(message, position + V2(8f, height - 8f - i * 15f + scroll), Assets.fontTiny)
            }

            flush()
            ScissorStack.popScissors();
        }
    }

    override fun update(delta: Float) {
        fishingMessageDelay -= delta

        scroll += scrollV
        scrollV *= 0.75f
        scroll = scroll.clamp(0f, Math.max(0, messages.size - ((height-18f) / 15f).toInt()) * 15f)

        super.update(delta)
    }

    fun append(rawMessage: String) {
        var message = rawMessage.replace("[^ -~]", "")

        if (message.matches(".*twice within the span of one second.*".toRegex())) {
            if(fishingMessageDelay <= 0f) {
                fishingMessageDelay += 5f
                val beneficial = PlayerPanel.myEffects.filter { it == "Vampirism" || it == "Lycanthropy" }
                message = ("Fishing for beneficial debuffs: " + (beneficial.firstOrNull()?.let {
                    "currently have: " + beneficial.joinToString(" and ")
                } ?: "none yet (${PlayerPanel.myEffects.size} bad ones)."))
            } else {
                return
            }
        }

        abilityGainedPattern.mapNotNull { it.matchEntire(message) }.firstOrNull()?.groupValues?.let { (_, ability) ->
            val newLevel = Controller.classLevels[Controller.currentClass]
            val abilityCooldown = when(newLevel) {
                1 -> 300
                3 -> 900
                5 -> 1800
                else -> null
            }
            AbilityPanel.learnAbility(Ability(ability, Controller.currentClass, abilityCooldown))
        }

        levelUpPattern.mapNotNull { it.matchEntire(message) }.firstOrNull()?.groupValues?.let { (_, newLevel, className) ->
            newLevel.toIntTry()?.let { level ->
                Controller.classLevels += className to level
                Controller.prefs.putString("classLevels", Api.Mapper.writeValueAsString(Controller.classLevels))
                Controller.prefs.flush()
            }
        }

        val pointsPattern = pointsMessagePatterns.find { message.matches(it) }
        val toastPattern = toastMessagePatterns.find { message.matches(it) }

        if (toastPattern != null) {
            toastPattern.matchEntire(message)?.groupValues?.let { (_, gained, _) ->
                PlayerPanel.toastPoints(gained)
            }
        }

        if (hiddenMessagePatterns.find { message.matches(it) } != null) {
            // Do nothing
        } else if (pointsPattern != null) {
            pointsPattern.matchEntire(message)?.groupValues?.let { (_, gained, _, total) ->
                PlayerPanel.toastPoints(gained)
            }
            Api.write("data/pointsLog.txt", message, true)
        } else {
            val formatted = bonusItemPattern.mapNotNull { it.find(message) }.firstOrNull()?.groupValues?.let { (_, pre, id, name, post) ->
                val (item, fixed) = try {
                    Item(name, UUID.fromString(id), 0, "") to "$pre'$name' $post"
                } catch (e: IllegalArgumentException) {
                    Item(id, UUID.fromString(name), 0, "") to "$pre'$id' $post"
                }
                InventoryPanel.addToInventory(item)
                Api.write("data/inventory.txt", """{ "item": "${item.name}", "id": "${item.id}" },""", true)
                fixed
            } ?: message
            messages = listOf(formatted) + messages
            messages = messages.take(MaxMessages)
            Api.write("data/eventLog.txt", message, true)
        }
    }

    override fun scrolled(amount: Int): Boolean {
        if(containsPoint(mouseV())) scrollV += amount*1.5f

        return super.scrolled(amount)
    }
}
