package xyz.jmullin.daringhero.entity

import xyz.jmullin.daringhero.bot.Api
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.daringhero.bot.model.*
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.C
import xyz.jmullin.drifter.extensions.V2
import xyz.jmullin.drifter.extensions.minus
import xyz.jmullin.drifter.extensions.plus
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite

object AbilityPanel : Entity2D() {
    var abilities = Abilities(listOf())
    val classes = listOf(
        "Lawyer", "Galactic Knight", "1337 haxx0r", "Artisinal Fungal Farmer", "Beard",
        "Lycan Lich", "Timelord", "Superheroine", "Avante-Garde Oboist", "Next Door Neighbor"
    )

    var switchRetry = -1f
    var levelRetry = -1f
    var retryClass: String? = null
    var retryLevelClass: String? = null

    fun load() {
        val abilityPrefs = Controller.prefs.getString("abilities")
        Controller.currentClass = Controller.prefs.getString("currentClass") ?: "Artisinal Fungal Farmer"

        abilities = if (abilityPrefs.isEmpty()) Abilities(listOf()) else Api.Mapper.readValue(abilityPrefs, Abilities::class.java)

        LogPanel.append("${classes.size} classes known; ${abilities.entries.size} ${if(abilities.entries.size == 1) "ability" else "abilities"} available.")

        populateButtons(Controller.currentClass)
    }

    fun populateButtons(className: String) {
        children.forEach(Entity2D::remove)

        classes.forEach { tClass ->
            val classAbilities = abilities.entries.filter { it.classOrigin == tClass }

            var dY = (height-8f) / classAbilities.size
            if(dY > (width - 8f)*1.25f) dY = width - 8f

            val rows = classAbilities.sortedBy { it.name }

            rows.forEachIndexed { yI, ability ->
                val button = AbilityButton(ability, position + V2(4f, height - 4f - dY - dY * yI), V2(width-8f, dY))
                if(tClass != className) button.hidden = true
                buttons += button
                add(button)
            }
        }
    }

    fun switchClass(className: String) {
        Controller.currentClass = className
        buttons.filterNot { it.ability.classOrigin == className }.forEach {
            it.selected = false
            it.autoUse = false
            it.hidden = true
        }
        buttons.filter { it.ability.classOrigin == className }.forEach {
            it.hidden = false
        }
        Controller.prefs.putString("currentClass", className)
        Controller.prefs.flush()
    }

    fun learnAbility(ability: Ability) {
        abilities = Abilities((abilities.entries + ability).distinctBy { "${it.classOrigin} - ${it.name}" })
        Controller.currentClass = ability.classOrigin
        buttons.filterNot { it.ability.classOrigin == ability.classOrigin }.forEach {
            it.selected = false
            it.autoUse = false
        }
        populateButtons(ability.classOrigin)
        Controller.prefs.putString("abilities", Api.Mapper.writeValueAsString(abilities))
        Controller.prefs.flush()
    }

    var buttons = setOf<AbilityButton>()

    var abilitySelected: Ability? = null
    var stickySelection = false

    fun select(button: AbilityButton, sticky: Boolean = false) {
        button.selected = true
        stickySelection = sticky
        abilitySelected = button.ability
    }

    fun unselect() {
        if (!stickySelection) {
            buttons.forEach { it.selected = false }
            abilitySelected = null
        }
    }

    override fun update(delta: Float) {
        retryClass?.let {
            if(switchRetry > 0f) {
                switchRetry -= delta
                if(switchRetry <= 0f) {
                    switchRetry = -1f
                    retryClass = null
                    Api.switchClass(it)
                }
            }
        }

        retryLevelClass?.let {
            if(levelRetry > 0f) {
                levelRetry -= delta
                if(levelRetry <= 0f) {
                    levelRetry = -1f
                    retryLevelClass = null
                    Api.levelUp(it)
                }
            }
        }

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)
        }

        super.render(stage)
    }

}
