package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import xyz.jmullin.daringhero.Assets
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.C
import xyz.jmullin.drifter.extensions.V2
import xyz.jmullin.drifter.extensions.minus
import xyz.jmullin.drifter.extensions.plus
import xyz.jmullin.drifter.rendering.Draw
import xyz.jmullin.drifter.rendering.RenderStage
import xyz.jmullin.drifter.rendering.sprite
import xyz.jmullin.drifter.rendering.string

object Console : Entity2D() {
    var text = ""
    var textCompleted = ""
    var textOverlay = ""

    override fun render(stage: RenderStage) {
        if(position.y + size.y <= 0f) return

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            Assets.fontTiny.color = Color.WHITE
            string(text, position + V2(8f, 8f), Assets.fontTiny, V2(1f))
            Assets.fontTiny.color = Color.CYAN
            string(textOverlay, position + V2(8f, 8f), Assets.fontTiny, V2(1f))
        }
    }

    override fun update(delta: Float) {
        if(text.isNotBlank()) {
            position.y += (-6f - position.y) / 5f
        } else {
            position.y += (-LogPanel.height - position.y) / 5f
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            val command = text.trim().toLowerCase()
            when(command) {
                "exit" -> Gdx.app.exit()
            }

            text = ""
            textCompleted = ""
            textOverlay = ""
        }

        super.update(delta)
    }

    override fun containsPoint(v: Vector2) = true

    override fun keyTyped(character: Char): Boolean {
        text += character

        return super.keyTyped(character)
    }
}