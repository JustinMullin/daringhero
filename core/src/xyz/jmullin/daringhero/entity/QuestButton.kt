package xyz.jmullin.daringhero.entity

import com.badlogic.gdx.math.Vector2
import xyz.jmullin.daringhero.*
import xyz.jmullin.daringhero.bot.Api
import xyz.jmullin.daringhero.bot.Controller
import xyz.jmullin.drifter.entity.Entity2D
import xyz.jmullin.drifter.extensions.*
import xyz.jmullin.drifter.rendering.*
import xyz.jmullin.daringhero.shader.HeroShaders.cooldownShader
import java.time.LocalTime

object QuestButton : Entity2D() {
    val icon by lazy { Icons.getIcon("quest") }

    var selected = false

    var hoverGlow = 0f
    var clickGlow = 0f
    var selectedGlow = 0f
    var chargedGlow = 0f

    var autoUse = false

    var hoverTime = 0f

    val duration = 60.minutes
    var lastTimeRemaining = -1L
    var timeRemaining: Long = Long.MAX_VALUE
    val charging: Boolean get() = Controller.currentQuest == null && timeRemaining > 0

    var pulse = 0f

    var badRequest = false
    var badRequestDelay = 0f

    override fun update(delta: Float) {
        pulse += delta

        if (containsPoint(mouseV())) {
            hoverGlow += (0.15f - hoverGlow) / 5f
            hoverTime += delta
        } else {
            hoverGlow -= hoverGlow / 5f
            hoverTime = 0f
        }

        if (selected || autoUse) {
            selectedGlow += (0.25f - selectedGlow) / 5f
        } else {
            selectedGlow -= selectedGlow / 5f
        }

        clickGlow -= clickGlow / 7f
        chargedGlow -= chargedGlow / 30f
        if(lastTimeRemaining > 0f && timeRemaining <= 0f) {
            chargedGlow = 1f
            if(LocalTime.now().isAfter(LocalTime.of(7, 0)) &&
                LocalTime.now().isBefore(LocalTime.of(22, 0))) {
                notify("New quest available.", "A new quest can now be accepted.")
            }
        }
        lastTimeRemaining = timeRemaining

        badRequestDelay -= delta
        if(badRequestDelay <= 0f && badRequest) {
            badRequest = false
            if(!Api.quest()) {
                badRequest = true
                badRequestDelay = 0.5f
            }
        }

        super.update(delta)
    }

    override fun render(stage: RenderStage) {
        val glow =
            FloatMath.clamp(if (charging) {
                0.25f
            } else {
                0.4f + hoverGlow
            } + clickGlow + chargedGlow, 0f, 1f)

        val (a, b, c) = if (selected || autoUse) listOf(1f, 1f, 0.65f) else listOf(1f, 1f, 0.8f)

        val color = C(1f)

        stage.draw {
            Draw.fill.color = C(0.3f)
            sprite(Draw.fill, position, size)
            Draw.fill.color = C(0.2f)
            sprite(Draw.fill, position + 2f, size - 4f)
            Draw.fill.color = C(0.1f)
            sprite(Draw.fill, position + 4f, size - 8f)

            Shaders.switch(cooldownShader, this)
            val progress = if(charging) FloatMath.clamp(timeRemaining / duration.toFloat(), 0f, 1f) else 0f
            cooldownShader.program?.setUniformf("progress", progress)
            cooldownShader.program?.setUniformf("center", position + size/2f)

            Draw.fill.color = color.cpy() * glow * a
            sprite(Draw.fill, position + V2(4f), size - V2(8f))

            Draw.fill.color = color.cpy() * glow * c
            sprite(Draw.fill, position + V2(8f), size - V2(16f))

            val spriteOrigin = position + size / 2f - V2(20f)/2f

            icon.setAlpha(0.8f)
            sprite(icon, spriteOrigin, V2(20f))

            Shaders.switch(Shaders.default, this)
        }

        super.render(stage)
    }

    override fun touchDown(x: Int, y: Int, pointer: Int, button: Int): Boolean {


        return super.touchUp(x, y, pointer, button)
    }

    override fun touchUp(v: Vector2, pointer: Int, button: Int): Boolean {
        if (button == 0) {
            clickGlow = 1f
            if(!Api.quest()) {
                badRequest = true
                badRequestDelay = 0.5f
            }
        }
        if(button == 1) {
            Controller.currentQuest = null
            Controller.prefs.remove("currentQuest")
            Controller.questEnds = 0
            Controller.prefs.putLong("questEnds", Controller.questEnds)
        }

        return super.touchDown(v, pointer, button)
    }
}
