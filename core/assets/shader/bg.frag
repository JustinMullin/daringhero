//#version 330 core

#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP 
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;

uniform vec4 theme;

void main() {
  vec4 color = vec4(theme.rgb * 0.3, 1.0);
  gl_FragColor = color * texture2D(u_texture, v_texCoords);
}