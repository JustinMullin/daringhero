//#version 330 core

#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP 
#endif

#define PI 3.1415926535897932384626433832795

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;

uniform float progress;
uniform vec2 center;

void main() {
  float angle = acos(dot(vec2(1, 0), normalize(gl_FragCoord.xy - center))) / PI;
  float intensity = 0.6 + 0.4*ceil(angle - progress + 0.0001);
  gl_FragColor = intensity * v_color * texture2D(u_texture, v_texCoords);
}