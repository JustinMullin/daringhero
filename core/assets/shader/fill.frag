//#version 330 core

#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP 
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;

uniform vec4 theme;

bool onEdge(vec2 v) {
  return texture2D(u_texture, v_texCoords + vec2(0.5, 0.0)).a < 0.1 ||
    texture2D(u_texture, v_texCoords - vec2(0.5, 0.0)).a < 0.1 ||
    texture2D(u_texture, v_texCoords + vec2(0.0, 0.5)).a < 0.1 ||
    texture2D(u_texture, v_texCoords - vec2(0.0, 0.5)).a < 0.1;
}

void main() {
  vec4 color = vec4(theme.rgb * 0.8, 1.0);
  vec4 darkColor = vec4(theme.rgb * 0.1, 1.0);

  vec4 c = onEdge(v_texCoords) ? darkColor : color;
  gl_FragColor = c * texture2D(u_texture, v_texCoords);
}