package xyz.jmullin.daringhero.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import xyz.jmullin.drifter.extensions.GdxAlias
import xyz.jmullin.drifter.extensions.V2
import xyz.jmullin.daringhero.DaringHero

fun main(args: Array<String>): Unit {
    val config = LwjglApplicationConfiguration()

    if(args.contains("dev")) {
        DaringHero.devMode = true
    }

    config.title = "DaringHero"
    config.width = 1400
    config.height = 800
//    config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width
//    config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height
//    config.fullscreen = true
    config.resizable = false

    config.vSyncEnabled = true
    config.foregroundFPS = 60
//    config.samples = 4

    GdxAlias.fixGameSize(V2(1400f, 800f))

    LwjglApplication(DaringHero, config)
}